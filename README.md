# Installation and use instructions:
1. [Install WAMP](https://bitbucket.org/rsschool/v3a/src/master/src/doc/WAMP_install_and_setup.docx)
2. [Import database](https://bitbucket.org/rsschool/v3a/src/master/src/doc/DB_import.txt)
3. Install node
4. Install all node modules with their dependencies specified in [package.json](https://bitbucket.org/rsschool/v3a/src/master/package.json) - you can use **npm install** command
5. Make folders relative to root project folder (/):
   1. /dist
   2. /src/logs
   3. ../logs
6. Copy folder **/credentials** to one level upper of the root project folder

# Working modes:
- for development with webpack (hot mode is enabled): run **npm run start** and goto http://localhost:12345
- for DBMS administration: goto http://localhost:12347
- for testing: run **npm run build** and goto https://localhost
- for deployment on Live: run **npm run build** and copy all content in '/dist' to your hosting with php and mysql support (database import needed, db file is /db/v3a_full.sql)

