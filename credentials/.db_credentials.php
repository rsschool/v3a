<?php
// when deploying on Live server, this file should be located outside the web server root folder, for security reasons
// the password should be changed on Live server to stronger
$credentials = array(
'host' => 'localhost',
'dbName' => 'v3a',
'user' => 'root',
'password' => 'v3a'
);
