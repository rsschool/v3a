import NodeBuilder from '../../utils/NodeBuilder';
import { showOverlay, hideOverlay } from './overlay';
import Tools from '../../utils/Tools';

const showModal = e => {
    const ident = e.target.dataset.modal;
    document.getElementById(ident).classList.add('modal_active');
    showOverlay();
};

function createButtons() {
    const fragment = document.createDocumentFragment();

    const buttonsCont = new NodeBuilder('div').class('buttons-cont').app(fragment).build();
    const buttonDownload = new NodeBuilder('button')
        .class('button')
        .attr('data-modal', 'upload')
        .inner('Create album from uploaded photo\'s')
        .app(buttonsCont)
        .build();
    const buttonGet = new NodeBuilder('button')
        .addId('setCollectorNicknameButton')
        .class('button')
        .attr('data-modal', 'get')
        .inner('Create album from stamps collection')
        .app(buttonsCont)
        .build();

    buttonDownload.addEventListener('click', showModal);
    buttonGet.addEventListener('click', showModal);

    return fragment;
}

function createUploadModal() {
    const fragment = document.createDocumentFragment();
    const modalUpload = new NodeBuilder('div').class('modal').addId('upload').app(fragment)
    .build();
    modalUpload.innerHTML = `        
        <div class="drop-area">
            <span class="modal__icon"></span>
            <form class="modal__content">
                <p>Drag and drop your files here or</p>
                <input type="file" id="fileElem" multiple accept="image/*">
                <label class="button-upload" id="uploadFiles" for="fileElem">Upload a file</label>
            </form>                                        
        </div>
        <div class="gallery"></div>
        <div class="button-cont">
            <button class="button modal__button" id="uploadClose">Close</button>
            <button class="button modal__button modal__button_invisible" id="uploadCreate">Create</button>
            <button class="button modal__button modal__button_light modal__button_invisible" id="uploadCancel">Cancel</button>                        
        </div>        
    `;

    const inputFile = fragment.querySelector('#fileElem');
    const dropArea = fragment.querySelector('.drop-area');
    const gallery = fragment.querySelector('.gallery');

    const buttonCreate = fragment.querySelector('#uploadCreate');
    const buttonClose = fragment.querySelector('#uploadClose');
    const buttonCancel = fragment.querySelector('#uploadCancel');

    const hideUploadModal = () => {
        modalUpload.classList.remove('modal_active');
        gallery.innerHTML = '';
        buttonCreate.classList.add('modal__button_invisible');
        buttonCancel.classList.add('modal__button_invisible');
        buttonClose.classList.remove('modal__button_invisible');
        hideOverlay();
    };

    buttonClose.addEventListener('click', () => {
        modalUpload.classList.remove('modal_active');
        hideOverlay();
    });

    buttonCancel.addEventListener('click', hideUploadModal);

    const previewFile = file => {
        const img = new NodeBuilder('img').class('user-object').app(gallery).build();
        img.file = file;

        const reader = new FileReader();
        reader.onload = e => { img.src = e.target.result; };
        reader.readAsDataURL(file);

        /* img.src = window.URL.createObjectURL(file);
        img.onload = e => {
            window.URL.revokeObjectURL(e.target.src);
        }; */
    };

    const handleFiles = filesArray => {
        filesArray.forEach(file => {
            if (!file.type.startsWith('image/')) return;
            previewFile(file);
        });
    };

    const showCreateCancelButtons = () => {
        buttonClose.classList.add('modal__button_invisible');
        buttonCreate.classList.remove('modal__button_invisible');
        buttonCancel.classList.remove('modal__button_invisible');
    };

    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, e => {
            e.preventDefault();
            e.stopPropagation();
        });
    });

    ['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, () => {
            dropArea.classList.add('drop-area_highlight');
        });
    });

    ['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, () => {
            dropArea.classList.remove('drop-area_highlight');
        });
    });

    dropArea.addEventListener('drop', e => {
        const files = Array.from(e.dataTransfer.files);
        if (files.length === 0) return;
        handleFiles(files);
        showCreateCancelButtons();
    });

    inputFile.addEventListener('change', e => {
        const files = Array.from(e.target.files);
        if (files.length === 0) return;
        handleFiles(files);
        showCreateCancelButtons();
    });

    return fragment;
}

function createModalInputDialog() {
    const div = document.createElement('div');
    div.setAttribute('class', 'steps');
    div.innerHTML = `
        <div class="steps__icon">1</div>               
        <p class="steps__description">Enter the stamp collector's nickname from <a href="https://colnect.com/en/collectors/list/collectibles/stamps/sort/by_collection" target="_blank">colnect.com</a>
        to get a list of stamps collections.</p>
        <p class="steps__description">For example, collector <span>Armen Beknazaryan [Arbek]</span> has nickname <span>Arbek</span> in the square brackets.
        If there are no square brackets in the name, then this name is a nickname.</p>
        <p class="steps__description">You can create your own stamp collections in <a href="https://colnect.com" target="_blank">Colnect</a> and create Virtual 3D Album for them.</p>
        <p class="steps__description">To see how it works, you can use nickname <span>Arbek</span> - this collector has perfect stamp collections.</p>
        <input type="text" id="nickname" placeholder="Enter nickname">
        <div class="steps__button" id="inputNickname"></div>
        <p class="modal__message"></p> 
    `;
    return div;
}

function createModalSelectDialog(collectorName, collections) {
    const stepsCont = document.createElement('div');
    stepsCont.setAttribute('class', 'steps');
    stepsCont.innerHTML = `
        <div class="steps__icon">2</div>               
        <p class="steps__description">Collector <span>${collectorName}</span>
        has <span>${collections.length}</span> public stamp collections containing less than 20,000 stamps. Choose one of them.</p>
        <div class="select-group">
            <div class="steps__button steps__button_back" id="backToInput"></div>
            <select class="select-collection">
                <option value="null" disabled selected hidden>Choose collection:</option>
            </select>
            <div class="steps__button" id="selectCollection"></div>
        </div>
        <p class="modal__message"></p> 
    `;

    const backButton = stepsCont.querySelector('#backToInput');
    backButton.addEventListener('click', () => {
        stepsCont.remove();
        document.querySelector('#get').prepend(createModalInputDialog());
    });

    const select = stepsCont.querySelector('.select-collection');
    collections.forEach((elem, index) => {
        new NodeBuilder('option').attr('value', index).inner(elem).app(select)
        .build();
    });

    return stepsCont;
}

function createModalComposeDialog(stampsNumber) {
    const div = document.createElement('div');
    div.setAttribute('class', 'steps');
    const string = (stampsNumber <= 1) ? `is <span>${stampsNumber}</span> stamp` : `are <span>${stampsNumber}</span> stamps`;
    div.innerHTML = `
        <div class="steps__icon">3</div>               
        <p class="steps__description">There ${string} in selected collection.<br>
        Would you like to compose album?<br><br>
        Note: netiquette with respect to Colnect sets a 10-page limit.</p>
    `;
    return div;
}

function createGetModal() {
    const fragment = document.createDocumentFragment();
    const modalGet = new NodeBuilder('div').class('modal').addId('get').app(fragment)
    .build();
    modalGet.innerHTML = `                
        <div class="button-cont">
            <button class="button modal__button" id="getClose">Close</button>
            <button class="button modal__button modal__button_invisible" id="getCreate">Create</button>
            <button class="button modal__button modal__button_light modal__button_invisible" id="getCancel">Cancel</button>                        
        </div>        
    `;
    modalGet.prepend(createModalInputDialog());

    const buttonCreate = fragment.querySelector('#getCreate');
    const buttonClose = fragment.querySelector('#getClose');
    const buttonCancel = fragment.querySelector('#getCancel');

    const hideGetModal = () => {
        modalGet.classList.remove('modal_active');
        buttonCreate.classList.add('modal__button_invisible');
        buttonCancel.classList.add('modal__button_invisible');
        buttonClose.classList.remove('modal__button_invisible');
        hideOverlay();

        document.querySelector('.steps').remove();
        modalGet.prepend(createModalInputDialog());
    };

    buttonCancel.addEventListener('click', hideGetModal);

    buttonClose.addEventListener('click', () => {
        modalGet.classList.remove('modal_active');
        hideOverlay();
    });

    return fragment;
}

function createShareModal(link) {
    const fragment = document.createDocumentFragment();
    const modalShare = new NodeBuilder('div').class('modal modal_share').addId('share').app(fragment)
    .build();
    modalShare.innerHTML = `        
        <div class="column-element">            
            <p class="modal__text">Share</p>
            <span class="button-cross"></span>                                         
        </div>
        
        <div class="column-element">
            <p class="steps__description">Copy this link and share it.<br>Anyone who has this link can see your album</p><br>
            <input type="text" id="inputLink">
            <button class="button button_copy"><img src="../assets/images/copy.svg" alt="copy">Copy</button>
        </div>        
    `;

    const copyText = modalShare.querySelector('#inputLink');
    copyText.value = link;

    const buttonCopy = modalShare.querySelector('.button_copy');
    buttonCopy.addEventListener('click', () => {
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */
        document.execCommand('copy');
    });

    const buttonClose = modalShare.querySelector('.button-cross');
    buttonClose.addEventListener('click', () => {
        document.querySelector('.modal_share').remove();
        hideOverlay();
    });

    return fragment;
}

const createAlbumItem = (id, content) => {
    const {
        type,
        title,
        date,
        pagesNumber,
        titleImage,
    } = content;

    const listItemHTML = `
    <div class="album-item__description">                   
        <div class="content">
            <p class="album-name">${title}</p>
            <p class="album-pages">${(pagesNumber <= 1) ? `${pagesNumber} page` : `${pagesNumber} pages`}</p>
        </div>
    </div>
    <div class="album-item__date">
        <p class="date">${Tools.getDate(Number(date))}</p>
    </div>
    <div class="buttons-cont">
        <button class="button album-item__button" data-button="share"><img src="../assets/images/share.svg" alt="share">Share</button>
        <button class="button album-item__button" data-button="edit"><img src="../assets/images/edit.svg" alt="edit">Edit</button>
        <button class="button album-item__button" data-button="delete"><img src="../assets/images/delete.svg" alt="delete">Delete</button>
    </div>
    <div class="album-item__icon">
        <div>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    `;

    const fragment = document.createDocumentFragment();
    const album = new NodeBuilder('div').class('album-item').attr('data-id', id).app(fragment)
    .build();
    album.innerHTML = listItemHTML;

    const shareButton = album.querySelector('[data-button="share"]');
    shareButton.addEventListener('click', () => {
        const link = Tools.generateLink(id);
        document.querySelector('.account').prepend(createShareModal(link));
        showOverlay();
    });

    const description = fragment.querySelector('.album-item__description');
    // const titleNode = fragment.querySelector('.album-name');
    // const pagesNode = fragment.querySelector('.album-pages');
    // pagesNode.textContent = (pagesNumber <= 1) ? `${pagesNumber} page` : `${pagesNumber} pages`;
    // const dateNode = fragment.querySelector('.date');

    // titleNode.textContent = title;
    // dateNode.textContent = date;

    switch (type) {
    case 'pictures': {
        new NodeBuilder('img').class('first-page').attr('src', titleImage).attr('alt', 'cover')
        .prep(description)
        .build();
        break;
    }
    case 'stamps': {
        const div = new NodeBuilder('div').class('first-page').prep(description).build();
        div.innerHTML = titleImage;
        break;
    }
    default:
        break;
    }

    return fragment;
};

function createUserAlbumsList(userAlbums = []) {
    const fragment = document.createDocumentFragment();
    const albumsCont = new NodeBuilder('div').class('albums-container').app(fragment).build();

    if (userAlbums.length === 0) {
        albumsCont.textContent = 'You still don\'t have any album, click on the button above to create your first album.';
        new NodeBuilder('span').prep(albumsCont).build();
    } else {
        userAlbums.forEach(item => {
            const { id, content } = item;
            const albumViewItem = createAlbumItem(id, content);
            albumsCont.append(albumViewItem);
        });
    }

    return fragment;
}

export {
    createButtons,
    createUploadModal,
    createGetModal,
    createModalInputDialog,
    createModalSelectDialog,
    createModalComposeDialog,
    createUserAlbumsList,
};
