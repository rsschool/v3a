export default function createUserNavigation(user) {
    const container = document.createElement('div');
    container.classList.add('account-nav');

    container.innerHTML = `<div class="user">
                            <img class="user__icon" src="../assets/images/profile.svg" alt="icon">
                            <div class="user__info">
                                <span class="user__name">${user.name}</span>
                                <span class="user__email">${user.email}</span>
                            </div>
                            <span class="user__button"></span>
                        </div>
                        <ul class="navigation">
                            <li class="navigation__text">My Account</li>
<!--                            <li class="navigation__item">                -->
<!--                                <img class="navigation__icon" src="../assets/images/person.svg" alt="icon">-->
<!--                                <span id="profileButton">Profile</span>-->
<!--                            </li>-->
                            <li class="navigation__item">                
                                <img class="navigation__icon" src="../assets/images/log-out.svg" alt="icon">
                                <span id="logoutButton">Log out</span>
                            </li>
                        </ul>`;

    const button = container.querySelector('.user__button');
    const nav = container.querySelector('.navigation');
    button.addEventListener('click', e => {
        e.stopPropagation();
        nav.classList.toggle('navigation_active');
        button.classList.toggle('user__button_turned');
    });

    document.addEventListener('click', () => {
        const navPopUp = document.querySelector('.navigation_active');
        if (navPopUp) {
            navPopUp.classList.remove('navigation_active');
            button.classList.remove('user__button_turned');
        }
    });

    return container;
}
