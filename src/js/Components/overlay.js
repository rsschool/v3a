import NodeBuilder from '../../utils/NodeBuilder';

const showOverlay = () => {
    new NodeBuilder('div').class('overlay').prep(document.querySelector('#app-root')).build();
    document.body.classList.add('no-scroll');
};

const hideOverlay = () => {
    document.querySelector('.overlay').remove();
    document.body.classList.remove('no-scroll');
};

export { showOverlay, hideOverlay };
