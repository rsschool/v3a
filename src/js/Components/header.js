export default () => {
    const header = document.createElement('header');
    header.innerHTML = `<div class="logo-container">
                            <div class="logo"></div>
                            <h1 class="title">MyAlbum</h1>
                        </div>`;
    return header;
};
