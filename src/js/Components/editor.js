import NodeBuilder from '../../utils/NodeBuilder';

const BACKGROUNDS = [
    'svetlyy-background.jpg',
    'kamen-background.jpg',
    'kraska-background.jpg',
    'ornament-background.jpg',
    'trava-background.jpg',
    'treshchiny-background.jpg',
    'wooden-background.jpg',
];

function createSettingsMenu(albumModel) {
    const cont = document.createElement('div');
    cont.setAttribute('class', 'settings');
    cont.innerHTML = `    
        <div class="settings__title">
            <h3>Customize Album</h3>
            <div class="buttons-cont">
                <button class="button settings__button" id="applyButton">Apply</button>
                <button class="button settings__button settings__button_light" id="discardButton">Discard</button>
            </div>
        </div>
        <div class="settings__wrapper">
            <div class="settings__elem">
                <div class="settings__appearance">
                    <img src="../assets/images/name.svg" alt="icon" class="settings__icon">
                    <span class="settings__name">Name</span>                    
                    <span class="settings__trigger"></span>
                </div>
                <div class="settings__popup settings__popup_name">
                    <p>Album Name:</p>
                    <input type="text" id="inputAlbumName">                        
                </div>
            </div>
            <div class="settings__elem">
                <div class="settings__appearance">
                    <img src="../assets/images/font.svg" alt="icon" class="settings__icon">
                    <span class="settings__name">Font</span>                    
                    <span class="settings__trigger"></span>
                </div>
                <div class="settings__popup settings__popup_font">
                    <p>Font Color:</p>
                    <input type="color" id="inputFontColor" value="${albumModel.content.properties.font.color}">                                            
                </div>
            </div>
            <div class="settings__elem">
                <div class="settings__appearance">
                    <img src="../assets/images/background.svg" alt="icon" class="settings__icon">
                    <span class="settings__name">Background</span>                    
                    <span class="settings__trigger"></span>
                </div>
                <div class="settings__popup settings__popup_background">                      
                </div>
            </div>
            <div class="settings__elem">
                <div class="settings__appearance">
                    <img src="../assets/images/sound.svg" alt="icon" class="settings__icon">
                    <span class="settings__name">Sound</span>                    
                    <span class="settings__trigger"></span>
                </div>
                <div class="settings__popup  settings__popup_sound">
                    <p>Turned Off:</p>
                    <input type="checkbox" id="soundOff" checked="${albumModel.content.properties.isSoundTurnedOff}">                           
                </div>
            </div>
        </div>
    `;

    cont.querySelector('#inputAlbumName').value = albumModel.content.title;

    const popUpBackGround = cont.querySelector('.settings__popup_background');
    BACKGROUNDS.forEach(image => {
        const [name, extension] = image.split('.');
        const src = `../../assets/images/${name}_small.${extension}`;
        new NodeBuilder('img').class('background-example').attr('src', src).attr('alt', 'texture')
        .attr('data-image', image)
        .app(popUpBackGround)
        .build();
    });

    const settingsWrapper = cont.querySelector('.settings__wrapper');
    settingsWrapper.addEventListener('click', e => {
        if (e.target.classList.contains('settings__trigger')) {
            const popUp = e.target.parentNode.nextElementSibling;
            if (e.target.classList.contains('settings__trigger_turned')) {
                const popUpHeight = popUp.offsetHeight;
                popUp.style.height = `${popUpHeight}px`;

                setTimeout(() => { popUp.style.height = '0px'; }, 0);
                e.target.classList.remove('settings__trigger_turned');
            } else {
                popUp.style.position = 'absolute';
                popUp.style.visibility = 'hidden';
                popUp.style.height = 'auto';
                const targetHeight = popUp.offsetHeight;

                popUp.style.height = '0px';
                popUp.style.position = '';
                popUp.style.visibility = '';

                setTimeout(() => { popUp.style.height = `${targetHeight}px`; }, 0);
                e.target.classList.add('settings__trigger_turned');
            }
        }

        if (e.target.classList.contains('background-example')) {
            const dataAttr = e.target.getAttribute('src').split('_')[0];
            const src = `${dataAttr}.jpg`;
            const img = new Image();
            img.src = src;
            img.onload = () => {
                document.querySelector('.preview').style.backgroundImage = `url("${src}")`;
            };
            if (document.querySelector('.background-example_active')) {
                document.querySelector('.background-example_active').classList.remove('background-example_active');
            }
            e.target.classList.add('background-example_active');
        }
    });

    const inputFontColor = cont.querySelector('#inputFontColor');
    inputFontColor.addEventListener('change', e => {
        e.stopPropagation();
        const nodes = document.querySelectorAll('.page__content');
        [...nodes].forEach(node => {
            node.style.color = e.target.value;
        });
    });

    return cont;
}

function createPreview(albumModel) {
    const div = document.createElement('div');
    div.setAttribute('class', 'preview');

    const src = `../../assets/images/${albumModel.content.properties.backgroundImage}`;
    div.style.backgroundImage = `url("${src}")`;
    return div;
}

export { createSettingsMenu, createPreview };
