import NodeBuilder from '../../utils/NodeBuilder';

const createLoader = container => {
    const wrapper = new NodeBuilder('div').class('loader-wrapper').app(container).build();
    new NodeBuilder('div').class('loader').app(wrapper).build();
};

const deleteLoader = container => {
    container.querySelector('.loader-wrapper').remove();
};

export { createLoader, deleteLoader };
