const creators = [
    { name: 'Palina Cetin', link: 'https://github.com/pacetin' },
    { name: 'Dmytro Bordun', link: 'https://github.com/Nickieros' },
];

export default () => {
    const footer = document.createElement('footer');
    footer.innerHTML = `<div class="rs-logo">
                            <a href="https://rs.school/poland/" target="_blank">
                                <img src="assets/images/rs_school_js.svg" alt="rs-school-logo">
                            </a>
                        </div>
                        <div class="author">
                            <span>Created by:</span>
                            ${creators.map(creator => `<a href="${creator.link}" target="_blank">${creator.name}</a>`).join(', ')}
                            <span>, 2021</span>
                        </div>`;
    return footer;
};
