const loginComponentHTML = `<form method="POST" class="login-form" id="loginForm" name="loginForm">
    <div class="form-group">
        <h4 class="login-form__title">Welcome back!</h4>
        <p class="login-form__subtitle">Log in to your MyAlbum account.</p>
    </div>
    <div class="form-group">
        <span class="form__text">Don't have an account?</span>
        <span class="form__link" id="register">Register now!</span>
    </div>
    <div class="form-group form-group_invisible">
        <span class="form__text">Already have an account?</span>
        <span class="form__link" id="login">Log in!</span>
    </div>
    <div class="form-group  form-group_name form-group_invisible">    
        <span class="form__icon"></span>
        <input value="" id="user" name="user" type="text" class="input-lg" placeholder="Name">
    </div>
    <div class="form-group form-group_email">    
        <span class="form__icon"></span>
        <input value="" id="email" name="email" type="email" class="input-lg" placeholder="Email address" required="">
    </div>
    <div class="form-group  form-group_password">
        <span class="form__icon"></span>
        <input value="" id="password" name="password" type="password" placeholder="Password" class="input-lg" required="">
    </div>
    <div class="form-group">
        <button name="loginButton" id="loginButton" class="form__button">Log in</button>
    </div>
    <div class="form-group" style="display: none">
        <span class="form__text">Forgot your password?</span>
        <span class="form__link" id="reset">Reset it.</span>   
    </div>
    <div class="form-group">
        <span class="form__text error-message"></span>
    </div>
    </form>`;

export default () => {
    const formCont = document.createElement('div');
    formCont.className = 'form-container';
    formCont.innerHTML = loginComponentHTML;

    const registerLink = formCont.querySelector('#register');
    const loginLink = formCont.querySelector('#login');
    const button = formCont.querySelector('.form__button');
    const nameGroup = formCont.querySelector('.form-group_name');

    registerLink.addEventListener('click', () => {
        registerLink.parentNode.classList.add('form-group_invisible');
        loginLink.parentNode.classList.remove('form-group_invisible');
        nameGroup.classList.remove('form-group_invisible');
        button.textContent = 'Register';
    });

    loginLink.addEventListener('click', () => {
        loginLink.parentNode.classList.add('form-group_invisible');
        registerLink.parentNode.classList.remove('form-group_invisible');
        nameGroup.classList.add('form-group_invisible');
        button.textContent = 'Log  in';
    });

    return formCont;
};
