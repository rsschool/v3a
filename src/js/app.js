import '../sass/reset.scss';
import '../sass/styles.scss';
import 'regenerator-runtime/runtime';
import 'core-js/stable';
import Model from './Model';
import View from './View';
import Controller from './Controller';

// import only for 'hot' mode (npm run start)
if (module.hot) {
    import('./tests/testBackendSupport');
}

window.addEventListener('load', () => {
    const model = new Model();
    const view = new View();
    const controller = new Controller();

    model.init(controller);
    view.init(controller);
    controller.init(model, view);
    
    controller.start();
});
