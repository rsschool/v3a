import NodeBuilder from '../../utils/NodeBuilder';

export default class Progress {
    constructor(startValue, maxValue) {
        this.startValue = startValue;
        this.maxValue = maxValue;
    }

    initProgressBar(container, initialValue = this.startValue, maxValue = this.maxValue) {
        const wrapper = new NodeBuilder('div').class('progress-wrapper').app(container).build();
        wrapper.innerHTML = `
            <p class="progress-text">loading...</p>    
            <progress max="${maxValue}" value="${initialValue}"></progress>
        `;
    }

    removeProgressBar(container) {
        container.querySelector('.progress-wrapper').remove();
    }

    update(current, total) {
        total = (total > this.maxValue) ? this.maxValue : total;
        document.querySelector('.progress-text').textContent = `loading ${current}/${total}`;
        document.querySelector('progress').setAttribute('value', current);
        document.querySelector('progress').setAttribute('max', total);
    }
}
