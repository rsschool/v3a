/* eslint-disable no-console */
import Tools from '../../../utils/Tools';

class ColnectCollectorSource {
    #MAX_PAGES_NUMBER_DOWNLOAD = 10;

    #nickname = '';

    #name = '';

    #countryFlagId = 0;

    #country = '';

    #avatarUrl = '';

    #collectionNames = [];

    #collectionIds = [];

    #currentCollectionIndex = 0;

    #currentCollectionPagesTotal = 0;

    #currentCollectionStampsTotal = 0;

    #errorMessage = '';

    #currentCollectionContent = [];

    #listeners = [];

    /** @return {string} */
    get nickname() {
        return this.#nickname;
    }

    /**
     * Return name OR nickname if name not exists
     * @return {string}
     */
    get name() {
        return Tools.capitalizeFirstLetter(this.#name || this.#nickname);
    }

    /**
     * Return name and nickname OR nickname only if name not exists
     * @return {string}
     */
    get fullName() {
        return Tools.capitalizeFirstLetter(this.#name ? `${this.name} [${this.nickname}]` : this.#nickname);
    }

    /** @return {string} */
    get country() {
        return this.#country;
    }

    /**
     * Return collectors country flag url according to its location in Colnect image static storage
     * @return {string}
     */
    get countryFlagUrl() {
        return this.getCountryFlagUrl(this.#countryFlagId);
    }

    /** @return {string} */
    get avatarUrl() {
        return this.#avatarUrl;
    }

    /** @return {string[]} */
    get collectionNames() {
        return this.#collectionNames;
    }

    /** @return {string[]} */
    get collectionIds() {
        return this.#collectionIds;
    }

    /** @return {number} */
    get currentCollectionIndex() {
        return this.#currentCollectionIndex;
    }

    /** @param {number} index */
    set currentCollectionIndex(index) {
        this.#currentCollectionIndex = index;
    }

    /** @return {number} */
    get currentCollectionPagesTotal() {
        return this.#currentCollectionPagesTotal;
    }

    /** @return {number} */
    get currentCollectionStampsTotal() {
        return this.#currentCollectionStampsTotal;
    }

    /**
     * Content of the selected collection: the stamp objects array
     * @type {{name: string, id: number, country: string, series: string, imageId: number, flagId: number}[]}
     */
    get currentCollectionContent() {
        return this.#currentCollectionContent;
    }

    /** @return {string} */
    get errorMessage() {
        return this.#errorMessage;
    }

    /**
     * Maximum allowed number of downloadable pages from Colnect collections, default: 10
     * @return {number}
     */
    get MAX_PAGES_NUMBER_DOWNLOAD() {
        return this.#MAX_PAGES_NUMBER_DOWNLOAD;
    }

    /**
     * Download collector info from Colnect and fill it in the appropriate fields
     * If false returned, the errorMessage in the instance is available for error explanation
     * @param {string} nickname - collector nickname
     * @return {Promise<boolean>}
     */
    async downloadCollectorInfo(nickname) {
        await fetch(`api/getData.php?object=handshake&timeStamp=${Date.now()}`);

        return fetch(`api/getData.php?object=collector&nickname=${nickname}&send_cookies=true&${Date.now()}`)
        .then(response => response.json())
        .then(json => {
            // TODO The error should be handled better. The error reasons:
            // 1. we banned by Colnect
            // 2. page does not exist
            // 3. communication error
            // 4. Colnect is down
            // 5. some other error in our BackEnd: incorrect BackEnd setup, something is broken there or hosting error
            if (![404, 200].includes(json.status.http_code)) throw Error('Server error'); // 404 is not an error: it's mean that collector not exist

            const documentColnect = new DOMParser().parseFromString(json.content, 'text/html').documentElement;
            const nodeInventory = documentColnect.querySelector('#collector_inventory');
            if (nodeInventory) { // if collector exist
                const nodeFlagLink = documentColnect.querySelector('.flag32');

                this.#nickname = nickname;
                this.#name = documentColnect.querySelector('.user_prof').textContent.replace(`[${nickname}]`, '').trim();
                // eslint-disable-next-line prefer-destructuring
                this.#countryFlagId = (nodeFlagLink.style.backgroundImage.match(/.*\/(\d+).*\.png/) || [0, 0])[1];
                this.#country = nodeFlagLink.title;
                this.#avatarUrl = documentColnect.querySelector('.collector_picture').src.replace('colnect.com//', '');

                const nodeStampsHeading = [...nodeInventory.querySelectorAll('h3')]
                .find(heading => heading.textContent === 'Stamps');
                if (nodeStampsHeading) { // if collector has some PUBLIC STAMP collections
                    // don't add collection if it has more than 20,000 stamps due to limitation on Colnect
                    const nodeCollectionsList = [...nodeStampsHeading.nextElementSibling.nextElementSibling.children]
                        .filter(node => +node.textContent.replace(/.+(\(.+\)).+/, '$1').replace(/[^\d]/g, '') < 20000);
                    this.#collectionNames = nodeCollectionsList.map(liElement => liElement.querySelector('a').textContent);
                    this.#collectionIds = nodeCollectionsList.map(liElement => liElement.querySelector('a').href.match(/.*\/(.+)\/.*$/)[1]);
                }
            }
            return true;
        })
        .catch(error => {
            this.#errorMessage = error.message;
            console.error('Get collector info: ', error.message);
            return false;
        });
    }

    /**
     * Get stamps from chosen collection and save them into collector.currentCollectionContent
     * @param {number} [pageNumber] - for internal usage ONLY, used for recursion inside
     * @return {Promise<boolean>}
     */
    async downloadStamps(pageNumber = 1) {
        const stampsQuantityPrevious = this.currentCollectionContent.length;
        return this._downloadCollectionPage(pageNumber)
            .then(isSuccessfulResponse => {
                if (!isSuccessfulResponse) throw Error(this.errorMessage);
                const stampsDownloadedQuantity = this.currentCollectionContent.length - stampsQuantityPrevious;
                this.notifyController();
                console.log(`${stampsDownloadedQuantity} stamps downloaded `
                    + `from page: ${pageNumber}/${this.currentCollectionPagesTotal}, `
                    + `total stamps downloaded: ${this.currentCollectionContent.length}/${this.currentCollectionStampsTotal}`);

                return pageNumber < this.MAX_PAGES_NUMBER_DOWNLOAD && pageNumber < this.currentCollectionPagesTotal
                    ? this.downloadStamps(pageNumber + 1)
                    : true;
            })
            .catch(error => {
                console.error('Get stamps collection: ', error.message);
                return false;
            });
    }

    /**
     * Download stamps from specified collection page and save them into collector.currentCollectionContent
     * @param {number} pageNumber
     * @return {Promise.<boolean>}
     */
    async _downloadCollectionPage(pageNumber) {
        return fetch('api/getData.php?'
            + 'object=page&'
            + `collector=${this.nickname}&`
            + `collection=${this.collectionIds[this.currentCollectionIndex]}&`
            + `pageNumber=${pageNumber}&`
            + `send_cookies=true&${Date.now()}`)
        .then(response => response.json())
        .then(json => {
            // TODO The error should be handled better. The error reasons:
            // 1. we banned by Colnect
            // 2. page does not exist
            // 3. communication error
            // 4. Colnect is down
            // 5. some other error in our BackEnd: incorrect BackEnd setup, something is broken there or hosting error
            if (json.status.http_code !== 200) throw Error('Server error');

            const documentColnect = new DOMParser().parseFromString(json.content, 'text/html').documentElement;
            const nodePaginatorLastPageLink = documentColnect.querySelector('.navigation_box a:last-of-type');
            if (nodePaginatorLastPageLink) { // pager_control on the last page is not a link and does not have a page number
                this.#currentCollectionPagesTotal = parseInt(nodePaginatorLastPageLink.getAttribute('data-page'))
                    + (nodePaginatorLastPageLink.classList.contains('pager_control') ? 0 : 1);
            } else { // last page link in the paginator block not exist if collection is empty OR number of pages = 1
                this.#currentCollectionPagesTotal = 1;
            }
            const arNodeStamps = [...documentColnect.querySelectorAll('#plist_items .pl-it')];
            if (arNodeStamps.length) { // if stamps exist
                this.#currentCollectionContent.push(...arNodeStamps.map(nodeStamp => {
                    const nodeItemHeader = nodeStamp.querySelector('.item_header');
                    const matchedImageSrc = nodeStamp.querySelector('.item_thumb img').getAttribute('data-src')
                        .match(/.*\/(\d+\/\d+).*\.jpg/);
                    const nodeFlag = nodeStamp.querySelector('.flag16');
                    const nodeSeries = nodeStamp.querySelector('a[href*="/series/"]');
                    this.#currentCollectionStampsTotal = parseInt(documentColnect.querySelector('.navigation_box')
                        .firstElementChild.textContent.replace(',', '').match(/\d*$/)[0]);
                    return {
                        name: nodeItemHeader.textContent,
                        id: parseInt(nodeItemHeader.firstElementChild.href.match(/\/(\d+).*$/)[1]),
                        country: nodeFlag.parentElement.textContent,
                        series: String((nodeSeries && nodeSeries.parentElement.textContent) || ''),
                        imageId: parseInt((matchedImageSrc && matchedImageSrc[1].replace('/', '')) || 0),
                        flagId: parseInt((nodeFlag.style.backgroundImage.match(/.*\/(\d+).*\.png/) || [0, 0])[1]),
                    };
                }));
            } else {
                console.log(`${Tools.capitalizeFirstLetter(this.currentCollectionName)}" is empty, stamps not exist`);
            }
            return true;
        })
        .catch(error => {
            this.#errorMessage = error.message;
            return false;
        });
    }

    /**
     * Return url for specified country flag id, according to its location in Colnect image static storage
     * @return {string}
     */
    getCountryFlagUrl(flagId) {
        return `https://i.colnect.net/flags/32/${(Tools.isNumber(flagId) && flagId > 0 && flagId) || 0}.png`;
    }

    /**
     * Return 'collection' if user selects the default first collection or "${collectionName}" collection
     * @return {string}
     */
    get currentCollectionName() {
        const collectionName = this.#collectionNames[this.#currentCollectionIndex];
        return Tools.capitalizeFirstLetter(collectionName);
    }

    notifyController() {
        this.#listeners.forEach(subs => subs(this.currentCollectionContent.length, this.currentCollectionStampsTotal));
    }
    
    subscribe(listener) {
        this.#listeners.push(listener);
    }
    
    unsubscribe(listener) {
        this.#listeners.filter(el => !(el instanceof listener));
    }
}

export default ColnectCollectorSource; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
