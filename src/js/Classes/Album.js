export default class Album {
    constructor(type) {
        this.type = type;
        this.title = 'Album';
        this.properties = {
            font: {
                family: 'Arial',
                size: '14',
                weight: 'normal',
                color: '#212529',
            },
            orientation: 'portrait',
            isSoundTurnedOff: true,
            backgroundImage: 'svetlyy-background.jpg',
        };
        this.titleImage = '';
        this.pagesNumber = 0;
        this.stampsObj = [];
        this.picturesURL = [];
        this.date = (new Date()).getTime();
        this.stampsPerPage = 9;
    }

    addPictures(pictures) {
        if (Array.isArray(pictures) && pictures.length !== 0) {
            this.picturesURL = [...pictures];
        }
    }

    addStamps(stamps) {
        if (Array.isArray(stamps) && stamps.length !== 0) {
            this.stampsObj = [...stamps];
        }
    }

    getPagesNumber() {
        let pages;
        if (this.type === 'pictures') {
            pages = this.picturesURL.length;
        } else if (this.type === 'stamps') {
            pages = Math.ceil(this.stampsObj.length / this.stampsPerPage);
            if (pages !== 0 && pages % 2 !== 0) {
                pages += 1;
            }
        }
        return pages;
    }

    static saveChanges(changesObj) {
        if (changesObj.albumName.trim() !== '') {
            this.title = changesObj.albumName.trim();
        }
        this.properties.font.color = changesObj.fontColor;
        if (changesObj.background) {
            this.properties.backgroundImage = changesObj.background;
        }
        this.properties.isSoundTurnedOff = changesObj.isSoundTurnedOff;
    }

    static groupStampsBySeries(array) {
        const map = new Map();
        return array.reduce((accum, current) => {
            const key = current.series;
            const collection = accum.get(key);
            const { name, imageId } = current;
            if (!collection) {
                accum.set(key, [{ name, imageId }]);
            } else {
                collection.push({ name, imageId });
            }
            return accum;
        }, map);
    }

    static sortStampsBySeries(array) {
        return array.sort((a, b) => {
            if (a.series === b.series) {
                return a.name < b.name ? -1 : 1;
            }
            return a.series < b.series ? -1 : 1;
        });
    }
}
