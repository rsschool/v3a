import Tools from '../../utils/Tools';

/**
 * Form validation class, supports chaining validation. All methods return class instance or throw first Error in the chain
 * @param {{userName: number, email: number, password: number}} [maxLength] - default to all is {@link #MAX_LENGTH} characters
 * @return {Object}
 * @throws {Error}
 * @example
 * const formValidator = new FormValidator(); // default max field length used
 * const formValidator = new FormValidator( { userName: 50 }); // default max field length used for all fields except userName
 * try {
 *     formValidator
 *         .validateUserName(sUserName) // or validateUserName(sUserName, 20) - set userName max length to 20
 *         .validateEmail(sEmail)
 *         .validatePassword(sPassword);
 * } catch (error) {
 *     // validation fail. Stop function execution
 *     nodeMessage.innerHTML = error.message;
 *     return;
 * }
 */
class FormValidator {
    // maximum characters number in a field = 40
    #MAX_LENGTH = 40;
    
    constructor(maxLength) {
        this._errorMessage = '';
        this.maxUserNameLength = (maxLength && maxLength.userName) || this.#MAX_LENGTH;
        this.maxEmailLength = (maxLength && maxLength.email) || this.#MAX_LENGTH;
        this.maxPasswordLength = (maxLength && maxLength.password) || this.#MAX_LENGTH;
        this.maxTextLength = (maxLength && maxLength.userName) || this.#MAX_LENGTH;
    }

    /**
     * Validate if email name matches its pattern and contains no more than (maxLength.email || this.#MAX_LENGTH) characters
     * @param {string} email
     * @param {number} [maxLength]
     * @return {FormValidator}
     * @throws {Error}
     */
    validateEmail(email, maxLength = this.maxEmailLength) {
        const regEmailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const valueName = 'email';
        // eslint-disable-next-line no-underscore-dangle
        return this._validateLength(maxLength, email, valueName)
        ._validatePattern(regEmailPattern, email, valueName);
    }

    /**
     * Validate if password not empty and contains no more than (maxLength.password || this.#MAX_LENGTH) characters
     * @param {string} password
     * @param {number} [maxLength]
     * @return {FormValidator}
     * @throws {Error}
     */
    validatePassword(password, maxLength = this.maxPasswordLength) {
        return this._validateLength(maxLength, password, 'password');
    }

    /**
     * Validate if userName not empty and contains no more than (maxLength.userName || this.#MAX_LENGTH) characters
     * @param {string} userName
     * @param {number} [maxLength]
     * @return {FormValidator}
     * @throws {Error}
     */
    validateUserName(userName, maxLength = this.maxUserNameLength) {
        return this._validateLength(maxLength, userName, 'name');
    }

    /**
     * Validate if text is not empty and contains no more than (maxLength.userName || this.#MAX_LENGTH) characters
     * @param {string} text
     * @param {number} [maxLength]
     * @return {FormValidator}
     * @throws {Error}
     */
    validateText(text, maxLength = this.maxUserNameLength) {
        return this._validateLength(maxLength, text, 'name');
    }

    /**
     * Validate if value not empty
     * @param {string} value - a value to check
     * @param {string} valueName - a value name to use in the error message
     * @return {FormValidator}
     * @throws {Error}
     * @private
     */
    _validateEmpty(value, valueName) {
        if (value.length === 0) throw Error(`${Tools.capitalizeFirstLetter(valueName)} shouldn't be empty`);
        return this;
    }

    /**
     * Validate maximum character length for a value
     * @param {number} maxLength - maximum character length for a value
     * @param {string} value - a value to check
     * @param {string} valueName - a value name to use in the error message
     * @return {FormValidator}
     * @throws {Error}
     * @private
     */
    _validateLength(maxLength, value, valueName) {
        this._validateEmpty(value, valueName);
        if (value.length > maxLength) throw Error(`${Tools.capitalizeFirstLetter(valueName)} must be less than ${maxLength} characters`);
        return this;
    }

    /**
     * Validate a value against a regExp pattern
     * @param {RegExp} regexpPattern
     * @param {string} value - a value to check
     * @param {string} valueName - a value name to use in the error message
     * @return {FormValidator}
     * @throws {Error}
     * @private
     */
    _validatePattern(regexpPattern, value, valueName) {
        if (!regexpPattern.test(value)) throw Error(`The text you entered doesn't look like ${valueName}`);
        return this;
    }
}

export default FormValidator; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
