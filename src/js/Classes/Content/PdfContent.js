import Content from './Content';

/**
 * PDF holder class
 */
class PdfContent extends Content {
    /**
     * @param {string} Url - URL to PDF file
     */
    constructor(Url) {
        super([Url], 1);
    }
}

export default PdfContent; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
