import Content from './Content';

/**
 * Image holder class
 */
class ImageContent extends Content {
    /**
     * @param {Array} arImagesUrl - an array of the images URL
     * @param {number} iNumberPerPage - images number per page
     */
    constructor(arImagesUrl, iNumberPerPage) {
        super(arImagesUrl, iNumberPerPage);
        this.content = '';
    }
}

export default ImageContent; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
