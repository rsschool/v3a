/**
 * Abstract class Content
 */
class Content {
    /**
     * @param {string[]|Object[]} arSources - the sources array, may be a string array for URLs or Object array for objects
     * @param {number} iNumberPerPage - items number per page
     */
    constructor(arSources, iNumberPerPage) {
        if (new.target === Content) {
            throw new Error(`Cannot construct ${new.target.name} instances directly`);
        }
        this.sources = arSources;
        this.numberPerPage = iNumberPerPage;
    }
}

export default Content; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
