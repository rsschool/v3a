/**
 * Test backend support and log result to console.
 */
async function testBackendSupport() {
    await fetch('/api/tests/testBackendSupport.php')
    .then(response => response.text())
    .then(text => {
        if (text !== 'BackEnd: PHP supported') throw Error(text);
        // eslint-disable-next-line no-console
        console.log(text);
    })
    .catch(error => {
        const errorMessage = typeof error === 'string' ? error : error.message;
        // eslint-disable-next-line no-console
        console.log(error);
        // eslint-disable-next-line no-alert
        alert(errorMessage);
    });
}

testBackendSupport();
