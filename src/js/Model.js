import ColnectCollectorSource from './Classes/Sources/ColnectCollectorSource';
// import Tools from '../utils/Tools';
import FormValidator from './Classes/FormValidator';

/**
 * Class Model - contains resources (data) and their status
 */
class Model {
    constructor() {
        this.controller = null;
        /**
         * Authentication status at Back-End
         * @type {boolean}
         * @private
         */
        this._isAuthenticated = false;

        /**
         * Current page
         * @type {{page: string}}
         * @private
         */
        this._currentPage = { page: '' };

        /**
         * Default page
         * @type {{page: string}}
         * @private
         */
        this._defaultPage = { page: 'account' };

        this._collector = null;

        this._user = {};
        this.albums = [];
        this.currentAlbum = null;
        this.language = 'en';
    }

    init(controller) {
        this.controller = controller;
    }

    /**
     * @param {Object} oAlbum
     */
    async createAlbum(oAlbum) {
        let sTitle = '';
        // TODO: refactor this shit in the code
        if (oAlbum.type === 'stamps') {
            // sTitle = `"${this.collector.currentCollectionName}" stamps collection
            // from collector ${Tools.capitalizeFirstLetter(this.collector.nickname)}`;
            // validate long title, e.g. collector "Javier-Gallego-Garcia" has "Ya intercambiado 26/10/2019" collection name
            // if (sTitle.length > 60) sTitle = `"${this.collector.currentCollectionName}" collection from ${this.collector.nickname}`;
            sTitle = this.collector.currentCollectionName === 'Collection'
                ? 'Stamps collection'
                : `"${this.collector.currentCollectionName}" stamps collection`;
            if (sTitle.length > 60) sTitle = `"${this.collector.currentCollectionName}" collection`;
            if (sTitle.length > 60) sTitle = `"${this.collector.currentCollectionName}"`;
            if (sTitle.length > 60) sTitle = 'Stamps Album';
        } else if (oAlbum.type === 'pictures') {
            sTitle = 'Photo album';
        }
        oAlbum.title = sTitle;
        return this._saveAlbum(oAlbum, 'create');
    }

    /**
     * Update (save) album in the backend after Apply button clicked
     */
    async updateAlbum() {
        const oAlbum = this.albums.find(album => album.id === this.currentPage.albumId).content;
        return this._saveAlbum(oAlbum, 'update');
    }

    /**
     * Save album at the database and in the model
     * @param {Object} oAlbum
     * @param {string} sAction - 'create' or 'update' action
     */
    async _saveAlbum(oAlbum, sAction) {
        if (!['create', 'update'].includes(sAction)) return false;
        const validator = new FormValidator();
        validator.validateText(oAlbum.title, 60);

        return fetch(
            './api/setData.php', {
                headers: { 'Content-Type': 'application/json;charset=utf-8' },
                method: 'POST',
                body: JSON.stringify({ action: sAction, data: oAlbum }),
            },
        )
            .then(response => response.json())
            .then(responseJson => {
                const response = JSON.parse(responseJson);
                if (response.errorMessage) throw Error(response.errorMessage);
                if (sAction === 'create') {
                    oAlbum.id = response.albumId;
                    this.albums.push({
                        id: response.albumId,
                        content: oAlbum,
                    });
                } else {
                    // in case if this album was deleted earlier (e.g. from other place), it was created again with another albumId
                    oAlbum.id = response.albumId;
                }
                localStorage.setItem(
                    oAlbum.id,
                    JSON.stringify({
                        title: oAlbum.title,
                        type: oAlbum.type,
                        pagesNumber: oAlbum.pagesNumber,
                        timeStamp: oAlbum.date,
                    }),
                );
                return response;
            })
            .catch(error => {
                console.error(error);
                return false;
            });
    }

    /**
     * @param {number} albumId
     */
    async deleteAlbum(albumId) {
        await fetch(
            './api/setData.php', {
                headers: { 'Content-Type': 'application/json;charset=utf-8' },
                method: 'POST',
                body: JSON.stringify({ action: 'delete', data: { albumId } }),
            },
        )
            .then(response => response.json())
            .then(responseJson => {
                const response = JSON.parse(responseJson);
                if (response.errorMessage) throw Error(response.errorMessage);
                const deletedAlbumIndex = this.albums.findIndex(elem => elem.id === albumId);
                if (deletedAlbumIndex !== -1) {
                    this.albums.splice(deletedAlbumIndex, 1);
                }
                return true;
            })
            .catch(error => {
                console.error(error);
                return false;
            });
    }

    /**
     * Return true or false if parameters not specified (auto-login with cookies is successful ? true or false)
     * Return string 'isLogged' if login successful with specified credentials or string with an error message
     * @param {string} [sEmail]
     * @param {string} [sPassword]
     * @returns {boolean|string} loginMessage
     */
    async login(sEmail = '', sPassword = '') {
        return fetch(
            './api/login.php', {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded;' },
                method: 'POST',
                body: `language=${this.language}&email=${sEmail}&password=${sPassword}`,
            },
        )
        .then(response => response.text())
        .then(loginMessage => {
            this._isAuthenticated = loginMessage === 'isLogged';
            return !sEmail && !sPassword ? this.isAuthenticated : loginMessage;
        })
        .catch(error => console.error('Login: ', error.message));
    }

    /**
     * Returns string 'isRegistered' or string with an error message after trying to register at back-end
     * @param {string} sUserName
     * @param {string} sEmail
     * @param {string} sPassword
     * @returns {Promise<string>} registerState
     */
    async register(sUserName, sEmail, sPassword) {
        return fetch(
            './api/register.php', {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded;' },
                method: 'POST',
                body: `language=${this.language}&userName=${sUserName}&email=${sEmail}&password=${sPassword}`,
            },
        )
        .then(response => response.text())
        .then(registerState => {
            this._isAuthenticated = registerState === 'isRegistered';
            return registerState;
        })
        .catch(error => console.error('Register: ', error.message));
    }

    /**
     * Saves current User data into model.user and albums list into model.albums
     */
    async getUserData() {
        await fetch('./api/getData.php?object=user')
            .then(response => response.json())
            .then(obj => [this._user] = obj)
            .catch(error => console.error('Get User data: ', error.message));
        await fetch('./api/getData.php?object=albumsList')
            .then(response => response.json())
            .then(albumList => this.albums = albumList.map(album => {
                localStorage.setItem(
                    album.id,
                    JSON.stringify({
                        title: album.title,
                        type: album.type,
                        pagesNumber: album.pagesNumber,
                        timeStamp: album.date,
                    }),
                );
                return { id: album.id, content: album };
            }))
            .catch(error => console.error('Get User data: ', error.message));
    }

    /**
     * Saves shared album into model.albums, there will be only one album
     * @param {string} albumId
     */
    async downloadSharedAlbum(albumId) {
        await this.downloadAlbum(albumId, true);
    }

    /**
     * Saves album into model.albums, there will be only one album
     * @param {string} albumId
     * @param {boolean} isShared - is album shared or regular
     */
    async downloadAlbum(albumId, isShared = false) {
        await fetch(`./api/getData.php?object=${isShared ? 'sharedAlbum' : 'album'}&albumId=${albumId}`)
            .then(response => response.json())
            .then(([albumFull]) => {
                const existedAlbum = this.albums.find(album => album.id === albumId);
                if (existedAlbum) {
                    existedAlbum.content = JSON.parse(albumFull.content);
                } else {
                    this.albums = [{ id: albumFull.id, content: JSON.parse(albumFull.content) }];
                }
            })
            .catch(error => console.error('Get album: ', error.message));
    }

    get isAuthenticated() {
        return this._isAuthenticated;
    }

    set currentPage(pageName) {
        this._currentPage = pageName;
    }

    get currentPage() {
        return this._currentPage;
    }

    set currentAlbum(oAlbum) {
        this._currentAlbum = oAlbum;
    }

    get currentAlbum() {
        return this.albums.find(album => album.id === this.currentPage.albumId);
    }

    getAlbumById(albumId) {
        return this.albums.find(album => album.id === albumId);
    }

    get defaultPage() {
        return this._defaultPage;
    }

    get user() {
        return this._user;
    }

    /** @type ColnectCollectorSource */
    get collector() {
        if (!this._collector) this._collector = new ColnectCollectorSource();
        return this._collector;
    }

    set collector(collector) {
        this._collector = collector;
    }
}

export default Model; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
