/* eslint-disable no-console,no-alert */
import FormValidator from './Classes/FormValidator';
import Album from './Classes/Album';
import Tools from '../utils/Tools';

class Controller {
    /**
     * @param {Object} model
     * @param {Object} view
     */
    constructor() {
        this.model = null;
        this.view = null;
        this.files = [];
    }

    init(model, view) {
        this.model = model;
        this.view = view;
    }

    static getSPAState() {
        const hashArray = window.location.hash.substr(1).split('_');
        const [page, albumId] = hashArray;
        return [page, albumId];
    }

    /**
     * Prepares and starts presentation
     */
    async start() {
        this._addEventListeners();

        const [pageName] = Controller.getSPAState();
        if (pageName === 'album') {
            this.switchToStateFromURLHash();
        } else {
            this.view.showLoader(document.querySelector('#app-root'));
            this.model.login() // async query to back-end: auto-login using cookies
            .then(isAuthenticated => {
                if (isAuthenticated) {
                    this.model.getUserData()
                    .then(() => {
                        this.switchToState({ page: 'account' });
                        this.view.hideLoader(document.querySelector('#app-root'));
                        this.switchToStateFromURLHash();
                    });
                } else {
                    this.switchToState({ page: 'login' });
                    this.switchToStateFromURLHash();
                }
            })
            .catch(error => console.error(error));
        }
    }

    /**
     * Change URL hash according SPA state
     * @param {{page: string, albumId: number}} spaState
     */
    switchToState(spaState) {
        const { page, albumId } = spaState;

        if (page === 'editor') {
            window.location.hash = `#${page}_${albumId}`;
        } else {
            window.location.hash = `#${page}`;
        }
    }

    /**
     * Change page to specified in URL hash.
     */
    async switchToStateFromURLHash() {
        const [page, albumId] = Controller.getSPAState();
        this.model.currentPage = (albumId === undefined) ? { page } : { page, albumId };

        if (page === 'album') {
            const oAlbum = this.model.getAlbumById(albumId);
            // TODO: bad model design - oAlbum.content.content, refactor this shit
            if (!oAlbum || !oAlbum.content.content) { // full album content not downloaded yet
                this.view.showLoader(document.querySelector('#app-root'));
                await this.model.downloadSharedAlbum(albumId);
                this.view.hideLoader(document.querySelector('#app-root'));
            }
        }

        this.view.renderPage(this.model.currentPage, this.model.user, this.model.albums);
    }

    _addEventListeners() {
        window.addEventListener('resize', () => {
            window.requestAnimationFrame(this.view.onResize.bind(this.view));
        });
        document.addEventListener('click', this.onUserAction.bind(this), { once: false });
        window.addEventListener('mousedown', this.onUserAction.bind(this));
        window.addEventListener('mouseup', this.onUserAction.bind(this));
        window.addEventListener('wheel', this.onUserAction.bind(this));
        window.addEventListener('keydown', this.onUserAction.bind(this));
        window.addEventListener('keyup', this.onUserAction.bind(this));
        window.addEventListener('change', this.onUserAction.bind(this));
        // window.addEventListener('mouseover', this.view.onMouseEvents.bind(this.view));
        // window.addEventListener('mouseout', this.view.onMouseEvents.bind(this.view));
        // window.addEventListener('mousemove', this.view.onMouseEvents.bind(this.view));
        window.addEventListener('hashchange', this.switchToStateFromURLHash.bind(this));
    }

    /**
     * User actions handler
     * @param {Object} event
     */
    onUserAction(event) {
        const { collector } = this.model;

        switch (event.type) {
        case 'keydown':
            if (event.key === 'Enter' && event.target.getAttribute('id') === 'nickname') {
                document.querySelector('#inputNickname').click();
            }
            break;
        case 'change':
            if (event.target.getAttribute('class') === 'select-collection') {
                this.view.hideModalMessage();
            }
            break;
        case 'click':
            switch (event.target.id) {
            case 'loginButton':
                event.preventDefault();
                event.target.blur();
                if (event.target.textContent === 'Register') this.register(event); else this.login(event);
                break;
            case 'logoutButton':
                event.target.blur();
                this.logout();
                break;
            case 'inputNickname': {
                const collectorNickname = document.querySelector('#nickname').value.trim();
                if (!collectorNickname) break;
                this.view.showLoader(document.querySelector('.modal_active'));

                collector.downloadCollectorInfo(collectorNickname)
                .then(isRequestSucceed => {
                    this.view.hideLoader(document.querySelector('.modal_active'));
                    if (isRequestSucceed) {
                        if (collector.nickname) { // collector exist
                            if (collector.collectionNames.length) { // collector has public stamp collections
                                this.view.showNextDialogStep('select', this.model.collector);
                            } else {
                                this.view.showModalMessage(`* Collector ${collector.fullName} has no public stamp collections or they have > 20.000 stamps`);
                            }
                        } else {
                            this.view.showModalMessage(`* Collector ${collectorNickname} not found in Colnect`);
                        }
                    } else {
                        throw Error(collector.errorMessage);
                    }
                })
                .catch(error => console.error(error.message));

                break;
            }

            case 'selectCollection':
                if (document.querySelector('.select-collection').value === 'null') {
                    this.view.showModalMessage('* You should choose some collection from list');
                    break;
                }
                collector.currentCollectionIndex = document.querySelector('.select-collection').value;

                this.view.showProgress(document.querySelector('.modal_active'));
                collector.subscribe(this.renewProgress.bind(this));

                collector.downloadStamps()
                .then(() => {
                    console.log('limitation: model.collector.MAX_DOWNLOADABLE_PAGES_NUMBER is set to 10');
                    console.log(`${collector.currentCollectionContent.length} stamps `
                    + 'successfully downloaded to collector.currentCollectionContent array');
                    console.log(`${Tools.capitalizeFirstLetter(collector.currentCollectionName)} collection has `
                                        + `${collector.currentCollectionStampsTotal} stamps in ${collector.currentCollectionPagesTotal} pages`);

                    collector.unsubscribe(this.renewProgress.bind(this));
                    setTimeout(() => {
                        this.view.hideProgress(document.querySelector('.modal_active'));
                        this.view.showNextDialogStep('create', collector);
                    }, 600);
                });
                break;
            case 'uploadCreate':
                this.createAlbum('pictures');
                break;
            case 'getCreate':
                this.createAlbum('stamps');
                this.model.collector = null;
                break;
            case 'getCancel':
                this.model.collector = null;
                break;
            case 'applyButton':
                this.view.showLoader(document.querySelector('#app-root'));
                this.saveChangesFromEditorToAlbumModel(this.model.currentPage.albumId);
                this.model.updateAlbum()
                    .then(() => {
                        this.view.hideLoader(document.querySelector('#app-root'));
                        this.switchToState({ page: 'account' });
                    });
                break;
            case 'discardButton':
                this.switchToState({ page: 'account' });
                break;
            default:
                break;
            }

            // handles events in albumsList
            if (event.target.classList.contains('album-item__button')) {
                const buttonType = event.target.dataset.button;
                const albumId = event.target.parentNode.parentNode.dataset.id;
                switch (buttonType) {
                case 'edit':
                    this.view.showLoader(document.querySelector('#app-root'));
                    this.model.downloadAlbum(albumId)
                        .then(() => this.switchToState({ page: 'editor', albumId }))
                        .finally(() => this.view.hideLoader(document.querySelector('#app-root')));
                    break;
                case 'delete':
                    this.deleteAlbum(albumId);
                    break;
                default:
                    break;
                }
            }

            // handles events in menu
            if (event.target.classList.contains('menuElement')) {
                if (event.target.id === 'logout') { // logout clicked
                    document.cookie = 'V3aSessionCookie=; expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/';
                    // this.view - очисть всё, потом спрячь лишнее и покажи окно логина;
                    // this.model - очисть все данные и переинициализируйся
                }
            }
            break;

        default:
            break;
        }
        return false;
    }

    /**
     * Try to log in. Validate user input and send request to the server.
     * The page will change to default if the login and password are successfully confirmed.
     * An error message will appear if validation fail
     */
    login(event) {
        const { nodeMessage, sEmail, sPassword } = this.getUserCredentials(event);
        const formValidator = new FormValidator();

        nodeMessage.innerHTML = '';

        // validate user input
        try {
            formValidator.validateEmail(sEmail).validatePassword(sPassword);
        } catch (error) {
            nodeMessage.innerHTML = error.message;
            nodeMessage.classList.add('error-message');
            nodeMessage.classList.remove('success-message');
            return;
        }

        this.view.showLoader(document.querySelector('#app-root'));
        // async query to back-end using user input
        this.model.login(sEmail, sPassword)
            .then(loginMessage => {
                if (loginMessage !== 'isLogged') {
                    nodeMessage.innerHTML = loginMessage;
                    nodeMessage.classList.add('error-message');
                    nodeMessage.classList.remove('success-message');
                    throw Error(loginMessage);
                } else {
                    this.model.getUserData()
                    .then(() => this.switchToState(this.model.defaultPage));
                }
            })
            .finally(() => {
                this.view.hideLoader(document.querySelector('#app-root'));
            });
    }

    /**
     * Clear session cookies and show login form
     */
    logout() {
        document.cookie = 'V3aSessionCookie=; expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/';
        window.location.reload();
    }

    /**
     * Try to register the user credentials. Validate user input and send request to the server.
     * The page will change to login form if the User credentials are successfully confirmed.
     * An error message will appear if registration fail
     */
    register(event) {
        // eslint-disable-next-line object-curly-newline
        const { nodeMessage, sUserName, sEmail, sPassword } = this.getUserCredentials(event);
        const formValidator = new FormValidator();

        nodeMessage.innerHTML = '';

        // validate user input
        try {
            formValidator.validateUserName(sUserName).validateEmail(sEmail).validatePassword(sPassword);
        } catch (error) {
            nodeMessage.innerHTML = error.message;
            nodeMessage.classList.add('error-message');
            nodeMessage.classList.remove('success-message');
            return;
        }

        // async query to back-end
        this.model.register(sUserName, sEmail, sPassword)
        .then(registerMessage => {
            const isError = registerMessage !== 'isRegistered';

            nodeMessage.innerHTML = isError ? registerMessage : 'Registration successful. Start to auto-login ...';
            nodeMessage.classList.toggle('error-message', isError);
            nodeMessage.classList.toggle('success-message', !isError);

            if (isError) {
                throw Error(registerMessage);
            } else {
                document.querySelector('#login').click();
                setTimeout(() => event.target.click(), 2000);
            }
        });
    }

    /**
     * Return an object with credential values and the node where the message might be displayed on login/register error/success
     * @param {Event} event
     * @return {{nodeMessage: HTMLElement, sUserName: string, sEmail: string, sPassword: string}}
     */
    getUserCredentials(event) {
        const nodeLoginForm = event.target.closest('.login-form');
        const nodeMessage = nodeLoginForm.querySelector('.error-message') || nodeLoginForm.querySelector('.success-message');
        const nodeUserName = nodeLoginForm.querySelector('#user');
        const nodeEmail = nodeLoginForm.querySelector('#email');
        const nodePassword = nodeLoginForm.querySelector('#password');
        return {
            nodeMessage,
            sUserName: nodeUserName ? nodeUserName.value : '',
            sEmail: nodeEmail ? nodeEmail.value : '',
            sPassword: nodePassword ? nodePassword.value : '',
        };
    }

    createAlbum(albumType) {
        const album = new Album(albumType);

        switch (albumType) {
        case 'pictures': {
            const userFileNodes = document.querySelectorAll('.user-object');
            const srcArray = [...userFileNodes].map(elem => elem.src);
            album.addPictures(srcArray);
            [album.titleImage] = srcArray;
            album.pagesNumber = album.getPagesNumber();
            break;
        }
        case 'stamps':
            album.addStamps(this.model.collector.currentCollectionContent);
            album.titleImage = `
            <svg xmlns='http://www.w3.org/2000/svg' viewBox="0 0 210 297">
                <defs>
                    pattern id="leather" patternUnits="userSpaceOnUse" width="50" height="50">
                        <image href="../assets/images/leather.png" x="0" y="0" width="50" height="50" />
                    </pattern>
                </defs>
            <rect x=0 y=0 width=210 height=297 stroke='#be955c' fill='#814921' />
            <rect x=0 y=0 width=210 height=297 fill="url(#leather)" />        
            <rect x=15 y=0 width=10 height=297 stroke='#be955c' fill='#8149219f' />
            <text x=40 y=175 fill=#814921 stroke=#be955c font-size=20 style="font-weight: 700; font-family: 'Helvetica'">Stamps Album</text>        
            </svg>
            `;
            album.pagesNumber = album.getPagesNumber();
            break;

        default:
            break;
        }

        this.view.showLoader(document.querySelector('.modal_active'));
        this.model.createAlbum(album)
            .then(response => {
                if (response.errorMessage) {
                    alert(response.errorMessage);
                } else {
                    this.view.renewUserAlbums(this.model.albums);
                }
            })
            .finally(() => {
                this.view.hideLoader(document.querySelector('.modal_active'));
                const modalId = document.querySelector('.modal_active').getAttribute('id');
                document.querySelector(`#${modalId}Cancel`).click();
            });
    }

    async deleteAlbum(albumId) {
        if (['5a3df52e', '06776982', '4f5f908e'].includes(albumId)) { // TODO: remove this restrictions for demo mode
            alert('Sorry, you can\'t delete the first three DEMO albums. You can create a new album and delete it');
            return;
        }
        this.view.showLoader(document.querySelector('#app-root'));
        await this.model.deleteAlbum(albumId);
        this.view.hideLoader(document.querySelector('#app-root'));
        this.view.renewUserAlbums(this.model.albums);
    }

    renewProgress(current, total) {
        this.view.renewProgress(current, total);
    }

    playSound(src) {
        Tools.playAudio(src);
    }

    saveChangesFromEditorToAlbumModel(currentAlbumId) {
        if (['5a3df52e', '06776982', '4f5f908e'].includes(currentAlbumId)) { // TODO: remove this restrictions for demo mode
            alert('Sorry, you can\'t save changes for the first three DEMO albums. You can create a new album and change it');
            return;
        }
        const albumName = document.querySelector('#inputAlbumName').value;
        const fontColor = document.querySelector('#inputFontColor').value;
        let background;
        if (document.querySelector('.background-example_active')) {
            background = document.querySelector('.background-example_active').dataset.image;
        } else background = undefined;

        const isSoundTurnedOff = document.querySelector('#soundOff').checked;

        const changesObj = {
            albumName,
            fontColor,
            background,
            isSoundTurnedOff,
        };

        const albumObj = this.model.albums.find(elem => elem.id === currentAlbumId);
        Album.saveChanges.call(albumObj.content, changesObj);
    }
}

export default Controller; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
