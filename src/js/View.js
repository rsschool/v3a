import { PageFlip } from '../libraries/page-flip.module';
import NodeBuilder from '../utils/NodeBuilder';
import createFooter from './Components/footer';
import createHeader from './Components/header';
import createLogin from './Components/login';
import {
    createButtons,
    createUploadModal,
    createGetModal,
    createModalSelectDialog,
    createModalComposeDialog,
    createUserAlbumsList,
} from './Components/account';
import { createSettingsMenu, createPreview } from './Components/editor';
import createUserNavigation from './Components/user';
import Album from './Classes/Album';
import Progress from './Classes/Progress';
import { createLoader, deleteLoader } from './Components/loader';
import Tools from '../utils/Tools';

class View {
    constructor() {
        this.controller = null;
        this.progress = null;
    }

    init(controller) {
        this.controller = controller;
    }

    /**
     * Create a wireframe for all elements and make their initial arrangement
     */
    renderPage(spaState, ...options) {
        const { page, albumId } = spaState;
        const [user, userAlbums] = options;
        const albumModel = userAlbums.find(album => album.id === albumId);

        const root = document.querySelector('#app-root');
        root.innerHTML = '';
        if (document.body.classList.contains('no-scroll')) {
            document.body.classList.remove('no-scroll');
        }

        const main = document.createElement('main');
        const header = createHeader();
        const footer = createFooter();

        const fragment = document.createDocumentFragment();

        switch (page) {
        case 'login':
        default:
            fragment.append(header);
            main.append(createLogin());
            fragment.append(main);
            fragment.append(footer);
            root.append(fragment);
            break;
        case 'account': {
            const section = new NodeBuilder('section').class(page).app(main).build();
            header.append(createUserNavigation(user));
            section.append(createButtons());
            section.append(createUploadModal());
            section.append(createGetModal());
            section.append(createUserAlbumsList(userAlbums));
            main.append(section);

            fragment.append(header);
            fragment.append(main);
            fragment.append(footer);
            root.append(fragment);
            break;
        }
        case 'editor': {
            const section = new NodeBuilder('section').class(page).app(main).build();
            section.append(createSettingsMenu(albumModel));
            section.append(createPreview(albumModel));
            section.querySelector('.preview').append(this.renderAlbum(albumModel));
            main.append(section);

            fragment.append(main);
            root.append(fragment);
            break;
        }
        case 'album': {
            const section = new NodeBuilder('section').class(page).app(main).build();
            const skin = new NodeBuilder('div').class('skin').app(main).build();
            if (!albumModel) {
                alert('Album not exist: it was deleted or shared link is wrong');
                return;
            }
            skin.style.backgroundImage = `url(../../assets/images/${albumModel.content.properties.backgroundImage})`;
            skin.append(this.renderAlbum(albumModel));
            main.append(section);

            fragment.append(main);
            root.append(fragment);
            break;
        }
        }
    }

    renewUserAlbums(userAlbums) {
        document.querySelector('.albums-container').remove();
        document.querySelector('.account').append(createUserAlbumsList(userAlbums));
    }

    /**
     * Show the finished interface to the user
     */
    show() {

    }

    /**
     * Repositioning and rescale of the elements, if needed
     */
    onResize() {
        // recalculate sizes when needed
    }

    /**
     * handles 'mouseover' and 'mouseout' events to change elements appearance
     * @param event
     */
    onMouseEvents(event) {
        switch (event.type) {
        case 'mouseover':
            break;

        case 'mouseout':
            break;

        case 'mousemove':
            break;

        default:
        }
    }

    renderAlbum(albumModel) {
        const settingsWidth = 320;
        const widthA4 = 210;
        const heightA4 = 297;

        const getClientSize = () => {
            const windowInnerWidth = document.documentElement.clientWidth;
            const windowInnerHeight = document.documentElement.clientHeight;
            return {
                width: windowInnerWidth,
                height: windowInnerHeight,
            };
        };

        const createAlbumFromHTML = () => {
            const { width, height } = getClientSize();
            const isClientRatioEnough = () => ((width - settingsWidth) / 2 / widthA4 * heightA4) < height;

            const settings = {
                width: widthA4,
                height: heightA4,
                size: 'stretch',
                minWidth: 200,
                maxWidth: width - settingsWidth,
                minHeight: 283,
                maxHeight: height,
                showCover: 'true',
                maxShadowOpacity: 0.5,
            };

            const wrapper = document.createElement('div');
            wrapper.setAttribute('class', 'album-wrapper');
            if (!isClientRatioEnough()) {
                const wrapperWidth = height * (widthA4 / heightA4) * 2;
                wrapper.style.maxWidth = `${wrapperWidth}px`;
            }

            const htmlParentElement = new NodeBuilder('div').class('flip-book').addId('album').app(wrapper)
            .build();
            let htmlFragment;
            if (albumModel.content.type === 'stamps') {
                htmlFragment = this.createHTMLFromStamps(albumModel);
            } else if (albumModel.content.type === 'pictures') {
                htmlFragment = this.createHTMLFromPictures(albumModel);
            }
            htmlParentElement.appendChild(htmlFragment);

            const pageFlip = new PageFlip(htmlParentElement, settings);
            pageFlip.loadFromHTML(htmlParentElement.querySelectorAll('.page'));
            /* pageFlip.on('flip', () => {
                this.controller.playSound('../assets/sounds/flip.mp3');
            }); */

            return wrapper;
        };

        return createAlbumFromHTML();
    }

    createHTMLFromPictures(albumModel) {
        const { picturesURL } = albumModel.content;
        const fragment = document.createDocumentFragment();
        picturesURL.forEach(elem => {
            const div = new NodeBuilder('div').class('page').app(fragment).build();
            new NodeBuilder('img').attr('src', elem).attr('alt', 'book-page').app(div)
            .build();
        });
        return fragment;
    }

    createHTMLFromStamps(albumModel) {
        const stamps = Album.sortStampsBySeries(albumModel.content.stampsObj);
        const { stampsPerPage } = albumModel.content;
        const pagesNumber = Math.ceil(stamps.length / stampsPerPage);

        const fragment = document.createDocumentFragment();

        const coverTop = new NodeBuilder('div').class('page cover cover_top').app(fragment).build();
        coverTop.innerHTML = `
            <div class="page__content page__content_cover">
                <div class="cover__bar"></div>
                <div class="cover__img"><img src="../assets/images/stamp.svg" alt="stamp" /></div>
                <div class="cover__title">${albumModel.content.title.replace(/((stamps)? collection)/, '<br>$1')}</div>
            </div>
        `;

        const coverTopBack = new NodeBuilder('div').class('page cover').app(fragment).build();
        coverTopBack.innerHTML = `
            <div class="cover__back"></div>
        `;

        for (let i = 0; i < pagesNumber; i++) {
            const page = new NodeBuilder('div')
                .class('page')
                .attr('data-density', 'hard')
                .app(fragment)
                .build();
            const pageCont = new NodeBuilder('div').class('page__content').app(page).build();
            pageCont.style.color = albumModel.content.properties.font.color;

            for (let j = 0; j < stampsPerPage; j++) {
                const index = i * stampsPerPage + j;
                if (index > stamps.length - 1) break;
                const stampModel = stamps[index];
                const stamp = new NodeBuilder('div').class('stamp').app(pageCont).build();
                const stampImageSrc = Tools.getColnectImageUrl(stampModel.imageId);
                stamp.innerHTML = `
                    <img class="stamp__img" src="${stampImageSrc}" alt="stamp">
                    <p class="stamp__title">${stampModel.name}</p>
                `;
            }
        }

        if (pagesNumber !== 0 && pagesNumber % 2 !== 0) {
            const pageExtra = new NodeBuilder('div').class('page').app(fragment).build();
            new NodeBuilder('div').class('page__content').app(pageExtra).build();
        }

        const coverBottomBack = new NodeBuilder('div').class('page cover').app(fragment).build();
        coverBottomBack.innerHTML = `
            <div class="cover__back cover__back_bottom"></div>
        `;

        new NodeBuilder('div').class('page cover cover_bottom').app(fragment).build();

        return fragment;
    }

    showLoader(container) {
        createLoader(container);
    }

    hideLoader(container) {
        deleteLoader(container);
    }

    showProgress(container) {
        this.progress = new Progress(0, 100);
        this.progress.initProgressBar(container);
    }

    renewProgress(current, total) {
        this.progress.update(current, total);
    }

    hideProgress(container) {
        this.progress.removeProgressBar(container);
    }

    showModalMessage(message) {
        const messageElem = document.querySelector('.modal_active .modal__message');
        if (messageElem) {
            messageElem.textContent = message;
        }
    }

    hideModalMessage() {
        const messageElem = document.querySelector('.modal_active .modal__message');
        if (messageElem) {
            messageElem.textContent = '';
        }
    }

    showNextDialogStep(step, collector) {
        document.querySelector('.steps').remove();
        const { fullName, collectionNames, currentCollectionContent } = collector;
        let stepNode;

        switch (step) {
        case 'select':
            stepNode = createModalSelectDialog(fullName, collectionNames);
            document.querySelector('#get').prepend(stepNode);
            break;
        case 'create':
            stepNode = createModalComposeDialog(currentCollectionContent.length);
            document.querySelector('#get').prepend(stepNode);
            document.querySelector('#getClose').classList.add('modal__button_invisible');
            document.querySelector('#getCreate').classList.remove('modal__button_invisible');
            document.querySelector('#getCancel').classList.remove('modal__button_invisible');
            break;
        default:
            break;
        }
    }
}

export default View; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
