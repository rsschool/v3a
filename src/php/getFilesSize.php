<?php
error_reporting(E_ALL | E_STRICT);//| E_STRICT );

require_once "./classes/Utils.php";
require_once './Classes/Translate.class.php';

$utils      = new Utils();
$translate  = new Translate();

// when input copied in $_POST, point (.) changed to underline (_)
// so we get data directly
// file_get_contents('php://input')            => $_POST
// parse_str($_SERVER['QUERY_STRING'], $files) => $_GET
// parse_str($_SERVER['HTTP_COOKIE'], $files)  => $_COOKIE
$files = json_decode(file_get_contents('php://input'));

// change '$files' array from {index => fileName} to {index => fileSize}
forEach ($files as $index => &$fileName) {
    $fileName = dirname(__DIR__) .DIRECTORY_SEPARATOR. $fileName;
    $fileName = str_replace('\\', '/', $fileName);
    $fileName = str_replace('..', '', $fileName);
    $fileName = str_replace('./', '/', $fileName);

    // validate filename on security reasons cos its from user-level environment
    if(!preg_match('/^[A-za-z0-9-:_\/]+\.[A-za-z0-9]+$/', $fileName))
        die('The file and its path "' . $fileName . '" does not match regex /^[A-za-z0-9-:_\/]+\.[A-za-z0-9]+$/');

    $fileName = filesize($fileName); // replace the fileName with its size
}

if(is_array(error_get_last()))
    $utils->setErrorMsgForDeveloper(
        $translate->dev_file_not_found."
        ".error_get_last()['message'].", 
        in file: ".error_get_last()['file'].", 
        at line: ".error_get_last()['line']);

echo json_encode($files,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
