<?php
error_reporting(error_reporting() & ~E_NOTICE);

class Proxy
{
	public function fetch($url)
	{
		//header("Access-Control-Allow-Origin: *");

		// config
		$enable_jsonp = false;
		$enable_native = false;
		$valid_url_regex = '/.*/';

		if (!$url) {
			// Passed url not specified.
			$content = 'ERROR: url not specified';
			$status = array('http_code' => 'ERROR');
		} else if (!preg_match($valid_url_regex, $url)) {

			// Passed url doesn't match $valid_url_regex.
			$content = 'ERROR: invalid url';
			$status = array('http_code' => 'ERROR');

		} else {

			$ch = curl_init($url);

			// @lf get domain from url and keep it around
			$parts = parse_url($url);
			$domain = $parts['scheme'] . "://" . $parts['host'];

			if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
			}

			if ($_GET['send_cookies']) {
				$cookies = [];
				foreach ($_COOKIE as $key => $value) {
					if ($key === 'V3aHandshake') $cookies[] = "cnv2sess=$value; Domain=$domain; path=/";
				}
				if ($_GET['send_session']) {
					$cookies[] = SID;
				}
				$cookies = implode('; ', $cookies);

				curl_setopt($ch, CURLOPT_COOKIE, $cookies);
			}

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_ENCODING, ""); // @lf guess encoding automatically

			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36');

			$result = curl_exec($ch);
			list($header, $content) = preg_split('/([\r\n][\r\n])\\1/', $result, 2);

			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
			$cookies = [];
			foreach ($matches[1] as $item) {
				parse_str($item, $cookie);
				$cookies = array_merge($cookies, $cookie);
			}

			foreach ($cookies as $key => $value) {
				if ($key === 'cnv2sess') setcookie('V3aHandshake', $value, time() + 2592000, '/');
			}

			// @lf filter any relative urls and replace them with absolute urls
			$rep['/href="(?!https?:\/\/)(?!data:)(?!#)/'] = 'href="' . $domain;
			$rep['/src="(?!https?:\/\/)(?!data:)(?!#)/'] = 'src="' . $domain;
			$rep['/href=\'(?!https?:\/\/)(?!data:)(?!#)/'] = 'href="' . $domain;
			$rep['/src=\'(?!https?:\/\/)(?!data:)(?!#)/'] = 'src="' . $domain;
			$rep['/@import[\n+\s+]"\//'] = '@import "' . $domain;
			$rep['/@import[\n+\s+]"\./'] = '@import "' . $domain;
			// @lf warning: clears previous contents
			$content = preg_replace(array_keys($rep), array_values($rep), $content);

			$status = curl_getinfo($ch);

			curl_close($ch);
		}

		// Split header text into an array.
		$header_text = preg_split('/[\r\n]+/', $header);

		if ($_GET['mode'] == 'native') {
			if (!$enable_native) {
				$content = 'ERROR: invalid mode';
				$status = array('http_code' => 'ERROR');
			}

			// Propagate headers to response.
			foreach ($header_text as $header) {
				if (preg_match('/^(?:Content-Type|Content-Language|Set-Cookie):/i', $header)) {
					header($header);
				}
			}

			print $content;

		} else {

			// $data will be serialized into JSON data.
			$data = array();

			// Propagate all HTTP headers into the JSON data object.
			if ($_GET['full_headers']) {
				$data['headers'] = array();

				foreach ($header_text as $header) {
					preg_match('/^(.+?):\s+(.*)$/', $header, $matches);
					if ($matches) {
						$data['headers'][$matches[1]] = $matches[2];
					}
				}
			}

			// Propagate all cURL request / response info to the JSON data object.
			if ($_GET['full_status']) {
				$data['status'] = $status;
			} else {
				$data['status'] = array();
				$data['status']['http_code'] = $status['http_code'];
			}

			// Set the JSON data object contents, decoding it from JSON if possible.
			$decoded_json = json_decode($content);
			$data['content'] = $decoded_json ? $decoded_json : $content;

			// Generate appropriate content-type header.
			$is_xhr = strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
			header('Content-type: application/' . ($is_xhr ? 'json' : 'x-javascript'));

			// Get JSONP callback.
			$jsonp_callback = $enable_jsonp && isset($_GET['callback']) ? $_GET['callback'] : null;

			// Generate JSON/JSONP string
			$json = json_encode($data);

			return $jsonp_callback ? "$jsonp_callback($json)" : $json;
		}
	}
}