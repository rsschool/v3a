<?php
/**
 * An associative array of translated languages​and their corresponding locales in the form {language code}-{country code} according to ISO.
 * For example, ru-UA is Russian in Ukraine, Ukrainian for 'the United Kingdom': uk-GB
 * Language codes: ISO 639-1, country codes: ISO 3166 Alpha-2
 *
 * Collision with ukrainian 'uk': The "Internet Assigned Numbers Authority" currently assigns the ccTLDs
 * mostly following the alpha-2 codes, but the United Kingdom, whose alpha-2 code is GB, uses .uk instead of .gb
 * as its ccTLD, as UK is currently exceptionally reserved in ISO 3166-1 on the request of the United Kingdom
 *
 * It is because of the collision with 'the United Kingdom' that we have the country code not 'UK', but 'UA',
 * which forms the locale code instead of the logical 'uk-UK' the illogical 'uk-UA',
 * and information about the Feng Shui 'ua-UA 'goes beyond the already bloated description
 * @property array[string $lang => string $code] $translatedLanguages
 * @see https://iso639-3.sil.org/code_tables/639/data/all?field_iso639_cd_st_mmbrshp_639_1_tid=255291&field_iso639_element_scope_tid=66&field_iso639_language_type_tid=51 ISO 639 language codes
 * @see https://www.iso.org/obp/ui/#search/code/ ISO 3166 country codes
 * @see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Imperfect_implementations Imperfect implementations for ISO 3166-1 alpha-2
 */
return $translatedLanguages = [
    'en' => 'en-GB',
//    'uk' => 'uk-UA',
    'ru' => 'ru-RU',
];
