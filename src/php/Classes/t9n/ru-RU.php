<?php
return (object) array(
    // messages for user
    'usr_credentials_required' => 'ERROR: имя пользователя и/или пароль пустые',
    'usr_credentials_incorrect' => 'ERROR: неизвестный пользователь или неправильный пароль',
    'usr_server_error' => 'ERROR: ошибка сервера',

    // messages for developer
    'dev_parameters_required' => 'GET/POST - ожидался параметр',
    'dev_language_required' => 'GET/POST - ожидалось "language"',
    'dev_object_required' => 'GET - ожидалось "object"',
    'dev_credentials_required' => 'Login credentials - имя пользователя и/или пароль отсутствуют или пустые',
    'dev_credentials_user_unknown' => 'Login credentials - неизвестный пользователь: ',
    'dev_credentials_password_incorrect' => 'Login credentials - неправильный пароль: ',
    'dev_login_required' => 'getData: необходим login',
    'dev_db_failed_connection' => 'Database - ошибка подключения: ',
    'dev_db_query_error' => 'Database - ошибка выполнения запроса: ',
    'dev_get_data_object_unknown' => 'GetData - неизвестный "object": ',
    'dev_file_not_found' => 'getFilesSize - файл не найден: ',
);
