<?php
return (object) array(
    // messages for user
    'usr_credentials_required' => 'ERROR: им\'я користувача та/або пароль порожні',
    'usr_credentials_incorrect' => 'ERROR: невідомий користувач абот неправильний пароль',
    'usr_server_error' => 'ERROR: помилка сервера',

    // messages for developer
    'dev_parameters_required' => 'GET/POST - очікувався параметр',
    'dev_language_required' => 'GET/POST - очікувалось "language"',
    'dev_object_required' => 'GET - очікувалось "object"',
    'dev_credentials_required' => 'Login credentials - им\'я користувача та/або пароль порожні або відсутні',
    'dev_credentials_user_unknown' => 'Login credentials - невідомий користувач: ',
    'dev_credentials_password_incorrect' => 'Login credentials - неправильний пароль: ',
    'dev_login_required' => 'getData: потрібен login',
    'dev_db_failed_connection' => 'Database - помилка підключення: ',
    'dev_db_query_error' => 'Database - помилка виконання запита: ',
    'dev_get_data_object_unknown' => 'GetData - невідомий "object": ',
    'dev_file_not_found' => 'getFilesSize - файл не знайдено: ',
);
