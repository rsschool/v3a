<?php
require_once "./Classes/Utils.php";

class Authenticator extends Utils {

    /**
     * @var Translate
     */
	private $translate;

    /**
     * @var bool
     */
	private $isLogged = false;

    /**
     * @var string
     */
	private $cookieName = 'V3aSessionCookie';

    /**
     * @var string
     */
	private $cookieExpired = '+1 month';

    /**
     * @var string
     */
	private $cookieRenew = '+10 minute';

    /**
     * @var Database
     */
	private $database;

    /**
     * Unique character combination for use in cookies
     * @var string
     * @see addSession
     */
	private $uniqueString = "V3aSessionCookie";

	private $hashLength = 40;

    /**
     * Authenticator constructor
     * @param Database $database
     * @param Translate $translate
     */
    public function __construct($database, $translate) {
        parent::__construct();
        $this->database = $database;
        $this->translate = $translate;

        $this->isLogged = $this->isLogged();
    }

    /**
     * Create a new session if $email and $password are validated.
     * Setting user cookies
     * @param $email
     * @param $password
     */
    public function login($email, $password) {
        $userId = $this->getUserId(strtolower($email));
        if ($userId === '')
			die($this->setErrorMsg(
			$this->translate->usr_login_credentials_incorrect.'',
			$this->translate->dev_login_credentials_email_unknown.$email));

		$this->verifyPassword($userId, $password);
        $this->addSession($userId);
        $this->isLogged = true;
    }

    /**
     * Return true if login is successful
     * @return boolean
     */
    public function isLogged() {
        if ($this->isLogged === false) {
            $this->isLogged = $this->checkSession($this->getCurrentSessionHash());
        }
        return $this->isLogged;
    }

    /**
     * Return hash for the current session or false
     * @return string
     * @return string|false
     */
    public function getCurrentSessionHash() {
        return $_COOKIE[$this->cookieName] ?? false;
    }

    /**
     * Check that session exist with specified hash
     * @param string $hash
     * @return boolean
     */
    public function checkSession($hash) {

        if (strlen($hash) != $this->hashLength)
            return false;

        $result = @$this->database->executeQuery(
                    "SELECT id, userId, ip, userAgent, expireDate, hash, cookieCRC FROM sessions WHERE hash = ?",
                    [$hash])[0];
        $userId = $result['userId'];
        $expireDate = strtotime($result['expireDate']);
        $currentDate = strtotime(date("Y-m-d H:i:s"));
        $ip = $result['ip'];
        $cookie = $result['cookieCRC'];

        if ($currentDate > $expireDate) {
            $this->deleteSessionsByHash($hash);
            return false;
        }

        if ($ip != $this->getIp())
            return false;

        if ($cookie == sha1($hash.$this->uniqueString)) {
            if ($expireDate - $currentDate < strtotime($this->cookieRenew) - $currentDate) {
                $this->deleteSessionsByHash($hash);
                $this->addSession($userId);
            }
            return true;
        }

        return false;
    }

    /**
     * Removes all existing sessions for the specified $userId
     * @param int $userId
     */
    private function deleteSessionsByUserId($userId)
    {
        $this->database->executeQuery(
            "DELETE FROM sessions WHERE userId = ?",
            [$userId]);
    }

    /**
     * Removes all existing sessions for $hash
     * @param int $hash
     */
    private function deleteSessionsByHash($hash)
    {
        $this->database->executeQuery(
            "DELETE FROM sessions WHERE hash = ?",
            [$hash]);
    }

    /**
     * Return the user ID
     * @param string $email
     * @return string
     */
    private function getUserId($email) {
        $result = $this->database->executeQuery(
            'SELECT id FROM users WHERE email = ? ',
            [$email]);
        return count($result) ? $result[0]['id'] : '';
    }

	/**
	 * Return the user ID with specified hash
	 * @param string $sessionHash
	 * @return string
	 */
	public function getUserIdBySessionHash($sessionHash) {
		$result = $this->database->executeQuery(
			'SELECT userId FROM sessions WHERE hash = ? ',
			[$sessionHash]);
		return $result[0]['userId'];
	}    

    /**
     * Checks hash and password match
     * @param string $userId
     * @param string $password
     */
    public function verifyPassword($userId, $password) {
        $result = $this->database->executeQuery('SELECT passwordHash FROM users WHERE id = ? ', [$userId])[0];

        if ( ! password_verify($password, $result['passwordHash']))
            die($this->setErrorMsg(
                $this->translate->usr_login_credentials_incorrect.'',
                $this->translate->dev_login_credentials_password_incorrect.
                "userId: $userId, password: $password"));
    }

	/**
	 * Return user password hash
	 * @param string $userId
	 * @return string
	 */
	public function getUserPasswordHash($userId) {
		return $this->database->executeQuery('SELECT passwordHash FROM users WHERE id = ? ', [$userId])[0]['passwordHash'];
	}

    /**
     * Creates a session with the specified userId
     * @param string $userId
     * @TODO update session if session exist instead deleting
     */
    protected function addSession($userId) {
        $ip = $this->getIp();

        $hash = sha1(microtime().$this->uniqueString.$this->getUserPasswordHash($userId).microtime().$this->uniqueString);
        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $language = $this->translate->getCurrentLanguage();
        $expireDate = date('Y-m-d H:i:s', strtotime($this->cookieExpired));
        $cookieCRC = sha1($hash.$this->uniqueString);

        $this->deleteSessionsByUserId($userId);
        // Create a new session
        $this->database->executeQuery(
            "INSERT INTO sessions
            (userId, ip, userAgent, language, expireDate, hash, cookieCRC)
            VALUES (?, ?, ?, ?, ?, ?, ?)",
            [
                $userId,
                $ip,
                $userAgent,
                $language,
                $expireDate,
                $hash,
                $cookieCRC
            ]);

        setcookie($this->cookieName, $hash, strtotime($expireDate), '/', NULL, '0', '0');
        $_COOKIE[$this->cookieName] = $hash;
    }

	/**
	 * Register user credentials
	 * @param string $userName
	 * @param string $email
	 * @param string $password
	 */
    public function register($userName, $email, $password) {
		if ($this->getUserId($email) !== '')
			die($this->setErrorMsg(
				$this->translate->usr_reg_credentials_exist.'',
				$this->translate->dev_reg_credentials_exist.$email));

		$passwordHash = password_hash($password, PASSWORD_ARGON2I, ['memory_cost' => 1024, 'time_cost' => 300, 'threads' => 1]);

		// Create new User
		$this->database->executeQuery(
			"INSERT INTO users
            (userName, email, groupId, passwordHash)
            VALUES (?, ?, ?, ?)",
			[
				$userName,
				$email,
				1,
				$passwordHash
			]);		
	}
	
	
}
