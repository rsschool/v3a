<?php
require_once "./Classes/Utils.php";

// returns an associative array $credentials[host,dbName,user,password]
// for security reasons, the file '.db_credentials.php' is located outside the web server root folder on production
require_once (dirname($_SERVER["DOCUMENT_ROOT"]) . '/../credentials/.db_credentials.php');
define('HOST', $credentials['host']);
define('DBNAME', $credentials['dbName']);
define('USER', $credentials['user']);
define('PASSWORD', $credentials['password']);

class Database extends Utils
{
    private $host = HOST;
    private $dbName = DBNAME;
    private $user = USER;
    private $password = PASSWORD;
    private $dataSourceName;
    private $charSet = "utf8";
    private $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'",
                ];

    /**
     * @var Translate
     */
    private $translate;

    /**
     * @var PDO
     */
    public $pdo;

    /**
     * Database constructor.
     * @param Translate $translate
     */
    public function __construct($translate)
    {
        parent::__construct();
        $this->translate = $translate;
        $this->dataSourceName = "mysql:host=$this->host;dbname=$this->dbName;charset=$this->charSet";

        try {
            $this->pdo = new PDO(
                $this->dataSourceName,
                $this->user,
                $this->password,
                $this->options);
        } catch (PDOException $error) {
            die($this->setErrorMsg(
                $this->translate->usr_server_error.'',
                $this->translate->dev_db_failed_connection.
                $error->getMessage().",  
                dataSourceName: $this->dataSourceName,
                user: $this->user, 
                password: $this->password, 
                options: ".implode($this->options)));
        }
    }

	/**
	 * @param string $query prepared statement with pseudo-parameters
	 * @param string[] $inputParameters (optional) an array of values ​​to substitute in the query instead of pseudo-parameters
	 * @return array|false $result array|false
	 */
	public function executeQuery($query, $inputParameters = ['']){
		try {
			$preparedQuery = $this->pdo->prepare($query);
			$preparedQuery->execute($inputParameters);
			// return fetchAll for 'select', in the other case (insert, delete) - true
			if (preg_match('/^\s*\bselect\b/i',$query))
				$result = $preparedQuery->fetchAll();
			else
				$result = false;
		} catch (PDOException $error) {
			die($this->setErrorMsg(
				$this->translate->usr_server_error.'',
				$this->translate->dev_db_query_error.
				$error->getMessage().",
                 query string: $query ;
                 input parameters:".PHP_EOL.'                 '
				.implode(''.PHP_EOL.'                 ',$inputParameters)));
		}
		return $result;
	}

	/**
	 * @param string $query prepared statement with pseudo-parameters
	 * @param string[] $inputParameters (optional) an array of values to substitute in the query instead of pseudo-parameters
	 * @return boolean
	 * @throws PDOException on error
	 */
	public function executeInsertQuery(string $query, $inputParameters = ['']): bool {
		try {
			$preparedQuery = $this->pdo->prepare($query);
			$result = $preparedQuery->execute($inputParameters);
			// TODO: $result will always be true when using UPDATE. This should be checked by $preparedQuery->rowCount()
			return true;
		} catch (PDOException $error) {
			if ($error->errorInfo[1] === 1062) return false; // duplicates found
			die($this->setErrorMsg(
				$this->translate->usr_server_error.'',
				$this->translate->dev_db_query_error.
				$error->getMessage().",
                 query string: $query ;
                 input parameters:".PHP_EOL.'                 '
				.implode(''.PHP_EOL.'                 ',$inputParameters)));
		}
	}
}
