<?php

require_once "./Classes/Utils.php";

/**
 * Class-holder for translations.
 * There is built-in 'en-GB' phrases that will be overwritten by translation specified in parameter.
 * The values are in the private fields, prefix usr/dev - the messages for user/developer.
 * If parameter omitted, ACCEPT_LANGUAGE from browser used.
 * If the language has no translations, the english will be used.
 * @see $translatedLanguages
 */
class Translate extends Utils
{
    public

    // messages for user
    $usr_server_error = 'Error: server error',
	$usr_login_required = 'Error: login required',
    $usr_login_credentials_required = 'Login error: email or password is empty',
	$usr_login_credentials_incorrect = 'Login error: unknown email or bad password',
	$usr_reg_credentials_incorrect = 'Register error: incorrect credentials',
	$usr_reg_credentials_exist = 'Register error: User with this email already exists',

    // messages for developer
    $dev_parameters_required = 'GET/POST - parameters required',
    $dev_language_required = 'GET/POST - "language" expected',
    $dev_object_required = 'GET - "object" expected',
    $dev_login_credentials_required = 'Login credentials - email and/or password absent or empty',
    $dev_login_credentials_email_unknown = 'Login credentials - unknown email ',
	$dev_login_credentials_password_incorrect = 'Login credentials - bad password: ',
	$dev_login_required = 'Error: login required ',
	$dev_reg_credentials_incorrect = 'Register error: incorrect credentials',
	$dev_reg_credentials_email_incorrect = 'Register error - email value doesn\'t look like email',
	$dev_reg_credentials_exist = 'Register error: User already exists with this email: ',
    $dev_db_failed_connection = 'Database - connection failed: ',
    $dev_db_query_error = 'Database - query failed: ',
    $dev_get_data_object_unknown = 'GetData - object unknown: ',
	$dev_get_data_collector_nickname_required = 'GetData - collector nickname required',
	$dev_get_data_collection_name_required = 'GetData - collection name required',
	$dev_get_data_page_number_required = 'GetData - page number required',
	$dev_get_handshake_timestamp_required = 'GetData - timestamp required for handshake',
	$dev_set_data_not_json = 'Set data - data is not in json format',
	$dev_set_data_action_not_found = 'Set data - action not found ',
	$dev_set_data_unknown_action = 'Set data - incorrect action: ',
	$dev_set_data_data_not_found = 'Set data - data not found',
	$dev_set_data_incorrect_data = 'Set data - incorrect data',
	$dev_set_data_delete_incorrect_albumId = 'Set data - incorrect albumId to delete: ',
	$dev_file_not_found = 'getFilesSize - file not found: ';

    /**
     * Default code for language and its country ( ISO 639-1 - ISO 3166 Alpha-2)
     * Used for file name formation with translations
     * @see $translatedLanguages
     */
    private $languageAndCountry = 'en-GB';

    /**
     * Specified by user language code that successfully validated using ISO 639-1
     * or default 'en' language used if validation fail
	 * @var string
     * @see $translatedLanguages
     */
    private $language = 'en';

	/** @var array */
    private $translatedLanguages = [];

    /**
     * Translate constructor.
     * @param string $language one of the languages in $translatedLanguages
     * @see Translate
     * @see $translatedLanguages
     */
    public function __construct($language = '') {
        parent::__construct();

        if ( ! $language) {
//			$language = substr(@$_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        	$language = 'en';
		}
        $language = $language == 'ua' ? 'uk' : $language; // if 'ua', then modify it according to ISO 639-1
        /**
         * An associative array of the translated languages and corresponding pair {language code}-{country code} according to ISO
         * Use it for 'require' without '_once', because it will be used twice (minimum twice)
         * @see $translatedLanguages
         */
        $this->translatedLanguages = require './Classes/t9n/translatedLanguages.php';

        // if the language not found, used 'en-GB', built in this class
        if (!isset($this->translatedLanguages[$language]))
            $this->setWarningMsgForDeveloper(
                "Translation initialize: translation language '$language' unavailable, defaulting to '$this->language'");
        else {
            $this->language = $language;
            $this->languageAndCountry = $this->translatedLanguages[$language];
        }

        // 'en-GB' translation already built in this class
        if ($this->languageAndCountry !== 'en-GB') {
            $translatedText = require "$this->languageAndCountry.php";

            // translation assigning
            foreach ($translatedText as $name => $value) {
                $this->$name = $value;
            }
        }
    }

    /**
     * Return a two-character code of the current language for translation
     * @return string
     */
    public function getCurrentLanguage() {
        return $this->language;
    }

    /**
     * Return an array of available languages
     * @return array
     */
    public function getAvailableLanguages() {
        return $this->translatedLanguages;
    }
};
