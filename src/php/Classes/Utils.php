<?php

/**
 * Class that stores errors and warnings,
 * auto-logging for errors and warnings,
 * service methods and other useful things
 */
class Utils
{
	/**#@+
	 * @access  private
	 */

	/**
	 * @access private
	 * @var string
	 */
	private $errorsForDeveloper = '';

	/**
	 * @access private
	 * @var string
	 */
	private $warningsForDeveloper = '';

	/**
	 * @access private
	 * @var string
	 */
	private $errorsForUser = '';

	/**
	 * @access private
	 * @var bool
	 */
	private $enabledErrorsForUser = false;

	/**
	 * @access private
	 * @var bool
	 */
	private $enabledErrorsForDeveloper = false;

	/**
	 * @access private
	 * @var bool
	 */
	private $enabledWarningsForDeveloper = false;
	/**#@-*/

	/**
	 * Utils constructor
	 */
	public function __construct()
	{
		$this->enableErrorsForUser();
		$this->enableErrorsForDeveloper();
		$this->enableWarningsForDeveloper();
		date_default_timezone_set("Europe/Kiev");
	}
	/** @noinspection PhpDocMissingThrowsInspection */
	/**
	 * Register and logging error for developer
	 * @param string $errorMessage
	 * @see enableErrorsForDeveloper(), disableErrorsForDeveloper()
	 */
	public function setErrorMsgForDeveloper($errorMessage)
	{
		if ($this->enabledErrorsForDeveloper) {
			/** @noinspection PhpUnhandledExceptionInspection */
			$errorMsg = (new DateTime("now", new DateTimeZone("Europe/Kiev")))->
				format("Y-m-d H:i:s") . ' ' . $this->getIp() . ' ERROR ' . $errorMessage . PHP_EOL;
			$this->errorsForDeveloper .= $errorMsg;
			$this->log($errorMsg);
		}
	}

	/** @noinspection PhpDocMissingThrowsInspection */
	/**
	 * Register and logging warning for developer
	 * @param string $warningMessage
	 * @see enableWarningsForDeveloper(), disableWarningsForDeveloper()
	 */
	public function setWarningMsgForDeveloper($warningMessage)
	{
		if ($this->enabledWarningsForDeveloper) {
			/** @noinspection PhpUnhandledExceptionInspection */
			$warningMsg = (new DateTime("now", new DateTimeZone("Europe/Kiev")))->
				format("Y-m-d H:i:s") . ' ' . $this->getIp() . ' WARNING ' . $warningMessage . PHP_EOL;
			$this->warningsForDeveloper .= $warningMsg;
			$this->log($warningMsg);
		}
	}

	/**
	 * Register and logging error for user
	 * @param string $errorMessage
	 * @see enableErrorsForUser(), disableErrorsForUser()
	 */
	public function setErrorMsgForUser($errorMessage)
	{
		if ($this->enabledErrorsForUser) {
			$this->errorsForUser .= $errorMessage . PHP_EOL;
		}
	}

	/**
	 * Register error for developer and user (aggregation)
	 * Logging error for developer
	 * @param string $forUser
	 * @param string $forDeveloper
	 * @return string $forUser
	 * @see setErrorMsgForUser(), setErrorMsgForDeveloper()
	 */
	public function setErrorMsg($forUser, $forDeveloper)
	{
		$this->setErrorMsgForUser($forUser);
		$this->setErrorMsgForDeveloper($forDeveloper);
		return $forUser;
	}

	/**
	 * Enable errors for user
	 */
	public function enableErrorsForUser()
	{
		$this->enabledErrorsForUser = true;
	}

	/**
	 * Enable errors for developer
	 */
	public function enableErrorsForDeveloper()
	{
		$this->enabledErrorsForDeveloper = true;
	}

	/**
	 * Enable warning for developer
	 */
	public function enableWarningsForDeveloper()
	{
		$this->enabledWarningsForDeveloper = true;
	}

	/**
	 * Disable errors for user
	 */
	public function disableErrorsForUser()
	{
		$this->enabledErrorsForUser = false;
	}

	/**
	 * Disable errors for developer
	 */
	public function disableErrorsForDeveloper()
	{
		$this->enabledErrorsForDeveloper = false;
	}

	/**
	 * Disable warnings for developer
	 */
	public function disableWarningsForDeveloper()
	{
		$this->enabledWarningsForDeveloper = false;
	}

	/**
	 * Returns all errors for user
	 * @return string|null
	 */
	public function getErrorsForUser()
	{
		return $this->errorsForUser;
	}

	/**
	 * Returns all errors for developer
	 * @return string|null
	 */
	public function getErrorsForDeveloper()
	{
		return $this->errorsForDeveloper;
	}

	/**
	 * Returns all warnings for developer
	 * @return string|null
	 */
	public function getWarningsForDeveloper()
	{
		return $this->warningsForDeveloper;
	}

	/**
	 * Returns all errors for user. This is a short name of the getErrorsForUser()
	 * @return string|null
	 */
	public function getErrors()
	{
		return $this->errorsForUser;
	}

	/**
	 * Check if errors exist
	 * @return bool
	 */
	public function isErrors()
	{
		return ($this->errorsForDeveloper || $this->errorsForUser) || false;
	}

	/**
	 * Returns IP
	 * @return string $ip
	 */
	public function getIp()
	{
		if (getenv('HTTP_CLIENT_IP')) {
			$ip = getenv('HTTP_CLIENT_IP');
		} elseif (getenv('HTTP_X_FORWARDED_FOR')) {
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		} elseif (getenv('HTTP_X_FORWARDED')) {
			$ip = getenv('HTTP_X_FORWARDED');
		} elseif (getenv('HTTP_FORWARDED_FOR')) {
			$ip = getenv('HTTP_FORWARDED_FOR');
		} elseif (getenv('HTTP_FORWARDED')) {
			$ip = getenv('HTTP_FORWARDED');
		} elseif (getenv('HTTP_CF_CONNECTING_IP')) {
			$ip = getenv('HTTP_CF_CONNECTING_IP');
		} elseif (getenv('HTTP_X_REAL_IP')) {
			$ip = getenv('HTTP_X_REAL_IP');
		} elseif (getenv('REMOTE_ADDR')) {
			$ip = getenv('REMOTE_ADDR');
		} else {
			$ip = '127.0.0.1';
		}
		return $ip;
	}

	/**
	 * Write errors to log file
	 * @param string $content
	 * @return bool
	 */
	public function log($content)
	{
		$path = dirname($_SERVER["DOCUMENT_ROOT"]).'/logs/';
		$filename = 'errors_php.log';
		$mode = 'a';
		if (!file_exists($path)) mkdir($path);
		if (!file_exists($path.$filename)) fclose(fopen($path.$filename, 'w'));
		$this->writeFile($path.$filename, $content, $mode);
		return true;
	}

	/**
	 * Write content to file. $mode: 'w','a'
	 * 
	 * 'w': open for writing only; place the file pointer at the beginning of the file and truncate the file to zero length. If the file does not exist, attempt to create it.
	 * 'a': open for writing only; place the file pointer at the end of the file. If the file does not exist, attempt to create it.
	 * @param string $filename
	 * @param string $content
	 * @param string $mode
	 * @return bool
	 */
	public function writeFile($filename, $content, $mode)
	{
		// is file exist and we can write to it?
		if (is_writable($filename)) {
			$handle = fopen($filename, $mode . 'b');
			if ($handle === false) {
				$this->setErrorMsgForDeveloper("Error: can't open the file ($filename)");
				return false;
			}
			if (flock($handle, LOCK_EX)) {  // acquire an exclusive lock
				if (fwrite($handle, $content) === FALSE) {
					$this->setErrorMsgForDeveloper("Error: can't write to file ($filename)");
				}
				fflush($handle);
//				flock($handle, LOCK_UN);
				fclose($handle);
				return !$this->isErrors();
			} else {
				$this->setErrorMsgForDeveloper("Error: file $filename is not writable");
				return false;
			}
		} else {
			$this->setErrorMsgForDeveloper("Error: can't lock the file $filename");
			return false;
		}
	}

	/**
	 * Read a file
	 * @param string $filename
	 * @return string
	 */
	public function readFile($filename)
	{
		// is file exist and we can read it?
		if (is_readable($filename)) {
			return file_get_contents($filename);
		} else {
			$this->setErrorMsgForDeveloper("Error: file $filename is not readable");
			return false;
		}
	}

	/**
	 * Delays execution of the script by the given time.
	 * @param int|float $time
	 * @example msleep(1.5); // delay for 1.5 seconds
	 * @example msleep(.1); // delay for 100 milliseconds
	 */
	public function msleep($time)
	{
		usleep($time * 1000000);
	}
}

