<?php
require_once './Classes/Utils.php';
require_once './Classes/Proxy.php';

Class Queue extends Utils {
	private int $failPageContentRequestsCount;
	private Proxy $proxy;

	public function __construct() {
		parent::__construct();
		$this->proxy = new Proxy();
		$this->failPageContentRequestsCount = 0; 
	}

	/**
	 * @param string $url
	 * @return bool
	 */
	public function getPageContent($url) {
		$lastRequestTimeStamp = $this->readFile('lastRequestTimeStamp');
		$currentTimeStamp = microtime(true);
		if (false !== $lastRequestTimeStamp
			&& ($currentTimeStamp - (float)$lastRequestTimeStamp) * 1000 > 2000
			&& $this->writeFile('lastRequestTimeStamp', microtime(true), 'w')) {
				$this->failPageContentRequestsCount = 0;
				return $this->proxy->fetch($url);
		}
		if ($this->failPageContentRequestsCount > 5) return false;
		$this->failPageContentRequestsCount++;
//		$this->setErrorMsgForDeveloper('fail requests count: '.$this->failPageContentRequestsCount.', url: '.$url);
		sleep(3);
		return $this->getPageContent($url);
	}
}
