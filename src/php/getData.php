<?php

//if(!@$_SERVER['HTTP_ACCEPT_LANGUAGE']) $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'uk'; // its empty in phpstorm

error_reporting(E_ALL | E_STRICT);//| E_STRICT );
header('Content-Type: application/json; charset=utf-8');

require_once './Classes/Utils.php';
require_once './Classes/Database.php';
require_once './Classes/Authenticator.php';
require_once './Classes/Translate.class.php';
require_once './Classes/Queue.php';

$utils = new Utils();
$translate = new Translate();

// check the values in the GET
if ( ! (@is_array($_GET) && count($_GET)))
    die($utils->setErrorMsg(
        $translate->usr_server_error.'',
        $translate->dev_parameters_required.''));

// change the browser language to specified if it translated
//if ( @$_GET["language"])
//    $translate= new Translate(substr($_GET['language'], 0, 2));

$language = $translate->getCurrentLanguage();

// check object
$object = $_GET["object"];

if (empty($object))
    die( $utils->setErrorMsg(
        $translate->usr_server_error.'',
        $translate->dev_object_required.''));

$database   = new Database($translate);
$auth       = new Authenticator($database, $translate);
if ('sharedAlbum' !== $object && !$auth->isLogged())
	die($utils->setErrorMsg(
		$translate->usr_login_required,
		$translate->dev_login_required.' for getData'));

// query preparing
$inputParameters = [''];
switch ($object) {

    case 'user':
        $tables     = 'users';
        $fields     = 'userName as name, email';
        $conditions = 'id='.$auth->getUserIdBySessionHash($auth->getCurrentSessionHash());
        break;

	case 'handshake':
		$timeStamp = @$_GET['timeStamp'];
		if (empty($timeStamp))
			die( $utils->setErrorMsg(
				$translate->usr_server_error.'',
				$translate->dev_get_handshake_timestamp_required.''));

		die((new Queue())->getPageContent("https://colnect.com/tool/say_hi?_=$timeStamp"));
		break;

	case 'collector':
		$nickname = @$_GET['nickname'];
		if (empty($nickname))
			die( $utils->setErrorMsg(
				$translate->usr_server_error.'',
				$translate->dev_get_data_collector_nickname_required.''));

		die((new Queue())->getPageContent("https://colnect.com/en/collectors/collector/$nickname"));
		break;

	case 'page':
		$nickname = @$_GET['collector'];
		if (empty($nickname))
			die( $utils->setErrorMsg(
				$translate->usr_server_error.'',
				$translate->dev_get_data_collector_nickname_required.''));
		$collection = @$_GET['collection'];
		if (empty($collection))
			die( $utils->setErrorMsg(
				$translate->usr_server_error.'',
				$translate->dev_get_data_collection_name_required.''));
		$pageNumber = @$_GET['pageNumber'];
		if (empty($pageNumber))
			die( $utils->setErrorMsg(
				$translate->usr_server_error.'',
				$translate->dev_get_data_page_number_required.''));
		die((new Queue())->getPageContent("https://colnect.com/en/stamps/list/$collection/$nickname/page/$pageNumber"));
		break;
	case 'albumsList':
		$tables     = 'albums';
		$fields     = 'albumId as id, type, date, title, pagesNumber, titleImage';
		$conditions = 'userId='.$auth->getUserIdBySessionHash($auth->getCurrentSessionHash());
		break;

	case 'album':
	case 'sharedAlbum':
		$tables     = 'albums a, albums_content ac';
		$fields     = 'a.albumId as id, a.type, a.title, a.pagesNumber, a.titleImage, a.date, ac.content';
		$conditions = 'a.albumId=? AND a.albumId = ac.albumId';
		$inputParameters    = [@$_GET['albumId']];
		break;

	// unknown object
    default:
        die($utils->setErrorMsg(
            $translate->usr_server_error.'',
            $translate->dev_get_data_object_unknown.$object));
}

// query assembling
$query = "
    SELECT
        $fields
    FROM
        $tables
    WHERE
        $conditions";

// execute query
$result = $database->executeQuery($query, $inputParameters);

echo json_encode($result,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
