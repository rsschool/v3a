<?php
/**
 * Finding the optimal time_cost value for hashing with the Argon2i algorithm
 * The time_cost value is selected so that on this hardware
 * the hashing algorithm lasts no more than a specified time (hashingDurationTarget).
 * Basically, this is a conversion of a given time in seconds into 'iterative' units of the algorithm (time_cost).
 * Several iterations are used to average the results.
 * there is Informing about the maximum spread and standard deviation
 * @author https://github.com/Nickieros
 */

$password = 'demo';
$hashingDurationTargetTime = 0.2;                  // Maximum hashing duration, sec.
$iterationQuantity = 5;                            // Number of iterations to improve accuracy, 3 ... 10 is recommended
$memoryCost = 1024;                                // the maximum amount of memory for calculations, KiB,
                                                   // but not less than 9 for one thread, 16 for 2, +8 for next
                                                   // default PHP constant: PASSWORD_ARGON2_DEFAULT_MEMORY_COST === 65536 
                                                   // a power of two value is recommended (16,32,64,128...)
$timeCost = PASSWORD_ARGON2_DEFAULT_TIME_COST;     // a time spent on computing (default: 4)
                                                   // unit of measure - iteration within the algorithm
$threads = PASSWORD_ARGON2_DEFAULT_THREADS;        // threads number for computing (default: 1)
$customOptions = [
    'memory_cost' => $memoryCost,
    'time_cost' => $timeCost,
    'threads' => $threads];

$hash = '';                 // there will be a computed hash for $password in final
$hashingDurationArray = []; // duration array of each iteration in hashBenchmark()

draftHashBenchmark("\r\n    First draft iteration");
draftHashBenchmark("\r\n    Second draft iteration");

// selecting/calculating a time_cost, the result will be more than the desired one, it will be necessary to subtract one back
do {
    hashBenchmark();
} while (array_sum($hashingDurationArray) / count($hashingDurationArray) < $hashingDurationTargetTime);

// average hash duration
$duration = array_sum($hashingDurationArray) / count($hashingDurationArray);

// maximum deviation
if (max($hashingDurationArray) - $duration > $duration - min($hashingDurationArray))
    $maxDeviation = max($hashingDurationArray) - $duration;
else
    $maxDeviation = $duration - min($hashingDurationArray);

// Casting to milliseconds
$duration = round(1000 * $duration,2);
$maxDeviation = round(1000 * $maxDeviation,2);
$averageDeviation = round( 1000 * standard_deviation($hashingDurationArray),2);


echo "
    Calculated time costs: $timeCost
    Hashing duration: $duration ms
    Maximum deviation: $maxDeviation%
    Standard deviation: $averageDeviation%
    
    The hashing will be executed $duration ms with following parameters:
    password_hash(
        '$password', // password
        PASSWORD_ARGON2I, // the Argon2i algorithm selected
        [
        'memory_cost' => $memoryCost, // maximum amount of memory for calculations (default: 65536)
        'time_cost'   => $timeCost, // time cost for calculations (default: 4)
        'threads'     => $threads // threads number for computing (default: 1)
        ]);
    
    Hash received:
    $hash";


/**
 * Calculating the standard deviation
 * @param $values - the values array
 * @return float - standard deviation
 */
function standard_deviation($values)
{
    $mean = array_sum($values) / count($values);
    $variance = 0.0;
    foreach ($values as $i)
    {
        $variance += pow($i - $mean, 2);
    }
    $variance /= count($values);
    return (float) sqrt($variance);
}

/**
 * Calculates a hash from $password and assigns a value to $hash
 * Increments $timeCost and updates $hashingDurationArray
 */
function hashBenchmark() {
    global $password, $memoryCost, $timeCost, $threads, $hash, $hashingDurationArray, $iterationQuantity;
    $hashingDurationArray = []; // an array of the durations of each pass
    $timeCost++;
    for ($i = 0; $i < $iterationQuantity; $i++) {
        $start = microtime(true);
        $hash = password_hash(
            "$password",
            PASSWORD_ARGON2I, [
            'memory_cost' => $memoryCost,
            'time_cost' => $timeCost,
            'threads' => $threads]);
        $duration = microtime(true) - $start;
        array_push($hashingDurationArray, $duration);
    }
}

/**
 * Fast but inaccurate calculation of time_cost, which will start more accurate calculation
 * This helps to get closer to real time_cost, reducing a calculations time
 * @param String echo_message
 */
function draftHashBenchmark($echo_message = "First draft iteration") {
    global $timeCost, $hashingDurationTargetTime, $hashingDurationArray;
    hashBenchmark(); // A small value for time_cost will give a large spread
    // average hash duration
    $duration = array_sum($hashingDurationArray) / count($hashingDurationArray);
    if ($hashingDurationTargetTime > $duration) {
        if ($timeCost > 2) $timeCost = round (--$timeCost * $hashingDurationTargetTime / $duration);
    }
    else {
		$timeCost = 0;
	}
	echo $echo_message.": time_cost = ".($timeCost === 0 ? 'lack of accuracy for specified hashing duration time' : $timeCost).PHP_EOL;
}
