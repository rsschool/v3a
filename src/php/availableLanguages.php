<?php
error_reporting(E_ALL | E_STRICT);//| E_STRICT );

require './classes/Utils.php';
require './Classes/Translate.class.php';

$utils      = new Utils();
$translate = new Translate();

echo json_encode(array_keys((array)$translate->getAvailableLanguages()),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
