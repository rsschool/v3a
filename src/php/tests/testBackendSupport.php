<?php
$error = error_get_last();
$error_message = 'BackEnd error';
if (!empty($error)) {
	$error_message .= ' message: '.$error['message'].', file: '.$error['file'].', line: '.$error['line'].', type: '.$error['type'];
	die($error_message);
} else {
	echo('BackEnd: PHP supported');
}
