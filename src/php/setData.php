<?php

error_reporting(E_ALL | E_STRICT);//| E_STRICT );

require_once './Classes/Utils.php';
require_once './Classes/Database.php';
require_once './Classes/Authenticator.php';
require_once './Classes/Translate.class.php';
require_once './Classes/Queue.php';

$utils = new Utils();
$translate = new Translate();

$postData = @file_get_contents('php://input');

// check the values in the POST
if (empty($postData))
	die($utils->setErrorMsg(
		$translate->usr_server_error.'',
		$translate->dev_parameters_required.''));

// change the browser language to specified if it translated
//if ( @$_GET["language"])
//    $translate= new Translate(substr($_GET['language'], 0, 2));

$language = $translate->getCurrentLanguage();
$jsonData = json_decode($postData, true);

if (empty($jsonData))
	die( $utils->setErrorMsg(
		$translate->usr_server_error.'',
		$translate->dev_set_data_not_json.''));

$database   = new Database($translate);
$auth       = new Authenticator($database, $translate);
if (!$auth->isLogged())
	die($utils->setErrorMsg(
		$translate->usr_login_required,
		$translate->dev_login_required.''));

$action = $jsonData['action'];
if (empty($action))
	die($utils->setErrorMsg(
		$translate->usr_server_error.'',
		$translate->dev_set_data_action_not_found.''));

if (!in_array($action, ['create', 'update', 'delete']))
	die($utils->setErrorMsg(
		$translate->usr_server_error.'',
		$translate->dev_set_data_unknown_action.$action));

$album = $jsonData['data'];

if (empty($album))
	die($utils->setErrorMsg(
		$translate->usr_server_error.'',
		$translate->dev_set_data_data_not_found.''));

$arAlbumFields = [];
switch ($action) {
	case 'edit':
		$arAlbumFields = ['id'];
	case 'create':
		$arAlbumFields = array_merge($arAlbumFields, ['type', 'title', 'date', 'pagesNumber', 'titleImage', 'stampsPerPage', 'properties', 'picturesURL', 'stampsObj']);
		break;
	case 'delete':
		$arAlbumFields = ['albumId'];
		break;
}

forEach($arAlbumFields as $fieldName) {
	if (!array_key_exists($fieldName, $album))
		die($utils->setErrorMsg(
			$translate->usr_server_error.'',
			$translate->dev_set_data_incorrect_data.''));
	if ($fieldName === 'type' || $fieldName === 'title') {
		if (mb_strlen($album[$fieldName]) === 0 || mb_strlen($album[$fieldName]) > 60)
			die($utils->setErrorMsg(
				$translate->usr_server_error.'',
				$translate->dev_set_data_incorrect_data.''));
	}
}
$userId = $auth->getUserIdBySessionHash($auth->getCurrentSessionHash());

/**
 * @param Object $database
 * @param Array $album
 * @param int $userId
 * @return string
 */
function createAlbum(object $database, Array $album, int $userId): string {
	$albumId = hash('crc32', $album['date']);
	// Create new album
	do {
		$result = $database->executeInsertQuery(
			"INSERT INTO albums
            (albumId, userId, type, title, date, pagesNumber, titleImage)
            VALUES (?, ?, ?, ?, ?, ?, ?)",
			[
				$albumId,
				$userId,
				$album['type'],
				$album['title'],
				$album['date'],
				$album['pagesNumber'],
				$album['titleImage'],
			]);
		if (!$result) { // duplicate creation time found in albumId
			$album['date'] = time();
			$albumId = hash('crc32', $album['date']);
		}
	} while (!$result);
	$album['id'] = $albumId;
	$result = $database->executeInsertQuery(
		"INSERT INTO albums_content
            (albumId, content)
            VALUES (?, ?)",
		[
			$albumId,
			json_encode($album)
		]);
	return $result ? '{"albumId": "'.$albumId.'"}' : '{"errorMessage": "server error"}';
}

// query preparing
$inputParameters = [''];
$userId = $auth->getUserIdBySessionHash($auth->getCurrentSessionHash());
switch ($action) {

	case 'create':
		$response = createAlbum($database, $album, $userId);
		break;

	case 'update':
		$albumId = $album['id'];
		$tables     = 'albums';
		$fields     = 'albumId, userId, type, title, date, pagesNumber, titleImage';
		$conditions = "albumId=? and userId=$userId";
		if (empty($database->executeQuery("SELECT EXISTS(SELECT 1 FROM $tables WHERE $conditions) as id", [$albumId])[0]['id'])) {
			$response = createAlbum($database, $album, $userId);
		} else {
			$result = $database->executeInsertQuery(
				"UPDATE $tables set userId=?, type=?, title=?, date=?, pagesNumber=?, titleImage=? WHERE $conditions",
				[
					$userId,
					$album['type'],
					$album['title'],
					$album['date'],
					$album['pagesNumber'],
					$album['titleImage'],
					$albumId,
				]);
			if (!empty($result)) $result = $database->executeInsertQuery("UPDATE albums_content set content=? WHERE albumId=?", [json_encode($album), $albumId]);
			$response = empty($result) ? '{"errorMessage": "server error"}' : '{"albumId": "' . $albumId . '"}';
		}
		break;

	case 'delete':
		$albumId = $album['albumId'];
		if (empty($albumId))
			die( $utils->setErrorMsg(
				$translate->usr_server_error.'',
				$translate->dev_set_data_delete_incorrect_albumId.$albumId));

		$database->executeQuery("DELETE FROM albums WHERE albumId = ?", [$albumId]);
		$database->executeQuery("DELETE FROM albums_content WHERE albumId = ?", [$albumId]);
		$response = '{"result": true}';
		break;

	// unknown object
	default:
		die($utils->setErrorMsg(
			$translate->usr_server_error.'',
			$translate->dev_set_data_unknown_action.$action));
}

echo json_encode($response,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
