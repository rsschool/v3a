<?php

error_reporting(E_ALL | E_STRICT);//| E_STRICT );

require_once './Classes/Utils.php';
require_once './Classes/Database.php';
require_once './Classes/Authenticator.php';
require_once './Classes/Translate.class.php';

$utils      = new Utils();
$translate  = new Translate();

// check the values in the POST
if ( ! @is_array($_POST) || @$_POST['language'] === '')
    die($utils->setErrorMsg(
            $translate->usr_server_error.'',
            $translate->dev_language_required.''));

// change the browser language to specified if it translated
//$translate  = new Translate(substr($_POST['language'], 0, 2));

$database   = new Database($translate);
$auth       = new Authenticator($database, $translate);

$userName    = @$_POST['userName'];
$email       = @$_POST['email'];
$password    = @$_POST['password'];

// validation section
if ( $userName === '' || $email === '' || $password === '') {
	die($utils->setErrorMsg(
		$translate->usr_login_credentials_required . '',
		$translate->dev_login_credentials_required . ''));
}
$regEmailPattern = '/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
forEach ([$userName, $email, $password] as $value) {
	if (mb_strlen($value) === 0 || mb_strlen($value) > 40)
		die($utils->setErrorMsg(
			$translate->usr_reg_credentials_incorrect . '',
			$translate->dev_reg_credentials_incorrect . ''));
}
if (!preg_match($regEmailPattern, $email))
	die($utils->setErrorMsg(
		$translate->usr_reg_credentials_incorrect . '',
		$translate->dev_reg_credentials_email_incorrect . ''));

$auth->register($userName, $email, $password);

echo 'isRegistered';
