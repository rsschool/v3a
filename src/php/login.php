<?php

error_reporting(E_ALL | E_STRICT);//| E_STRICT );

require_once './Classes/Utils.php';
require_once './Classes/Database.php';
require_once './Classes/Authenticator.php';
require_once './Classes/Translate.class.php';

$utils      = new Utils();
$translate  = new Translate();

// check the values in the POST
if ( ! @is_array($_POST) || @$_POST['language'] === '')
    die($utils->setErrorMsg(
            $translate->usr_server_error.'',
            $translate->dev_language_required.''));

// change the browser language to specified if it translated
//$translate  = new Translate(substr($_POST['language'], 0, 2));

$database   = new Database($translate);
$auth       = new Authenticator($database, $translate);

if (!$auth->isLogged()) { 
    // check credentials
    $email      = @$_POST['email'];
    $password   = @$_POST['password'];

	// TODO: make simple string validation for email and password values (characters length, email pattern etc.)
    if ( $email === '' || $password === '') {
		die($utils->setErrorMsg(
			$translate->usr_login_credentials_required . '',
			$translate->dev_login_credentials_required . ''));
	}
	forEach ([$email, $password] as $value) {
		if (strlen($value) === 0 || strlen($value) > 40)
			die($utils->setErrorMsg(
				$translate->usr_login_credentials_incorrect . '',
				$translate->usr_login_credentials_incorrect . ''));
	}

    $auth->login($email, $password);
}

echo 'isLogged';
