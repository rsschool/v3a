class Tools {
    /**
     * Capitalize first letter of specified text string
     * @param {string} text
     * @return {string}
     */
    static capitalizeFirstLetter(text) {
        return typeof text === 'string' && text.length ? text.charAt(0).toUpperCase() + text.slice(1) : '';
    }

    /**
     * Return true if n is a number since isNan() isn't reliable (spaces return false)
     * @param {number} n - possible number
     * @returns {boolean} true if number, else false
     */
    static isNumber(n) {
        // eslint-disable-next-line no-restricted-globals
        return !isNaN(n) && isFinite(n);
    }

    /**
     * Return item (stamp) url according to its location in Colnect image static storage
     * @param {number} imageId
     * @param {string} [imageSize] - one of three image sizes: b/f/t, where b - large, f - normal, t - small, default: 'f'
     * @return {string}
     */
    static getColnectImageUrl(imageId, imageSize = 'f') {
        let url;
        let size = ['b', 'f', 't'].includes(imageSize) ? imageSize : 'f';
        if (Tools.isNumber(imageId) && imageId > 100000) {
            // eslint-disable-next-line no-bitwise
            url = `https://i.colnect.net/${imageSize}/${imageId / 1000 | 0}/${String(imageId).slice(-3)}/image.jpg`;
        } else {
            size = size === 't' ? 'thumb' : 'full';
            url = `https://i.colnect.net/items/${size}/none-stamps.jpg`;
        }
        return url;
    }

    static generateLink(albumId) {
        return `${window.location.origin}/#album_${albumId}`;
    }

    static getDate(value) {
        const now = new Date(value);
        const year = now.getFullYear();
        const month = now.getMonth();
        const date = now.getDate();
        const transformMonth = number => {
            const MONTHS = [
                'January', 'February', 'March', 'April',
                'May', 'June', 'July', 'August',
                'September', 'October', 'November', 'December',
            ];
            return MONTHS[number];
        };
        return `${transformMonth(month)} ${date}, ${year}`;
    }

    static playAudio(url) {
        const audio = new Audio();
        audio.src = url;
        audio.load();
        audio.play();
    }
}

export default Tools; // this export way is chosen to shut up the PHPStorm 'Element is not exported' warning
