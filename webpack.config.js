const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

const isDevelopmentEnvironment = process.env.NODE_ENV === 'development';
const isProductionEnvironment = !isDevelopmentEnvironment;
const fileName = extension => (isDevelopmentEnvironment ? `[name].${extension}` : `[name].[fullhash].${extension}`);
const copyWebpackPluginPatterns = [
    {
        from: path.resolve(__dirname, 'src/assets/images'),
        to: path.resolve(__dirname, 'dist/assets/images'),
    },
    {
        from: path.resolve(__dirname, 'src/assets/ico'),
        to: path.resolve(__dirname, 'dist/assets/ico'),
    },
    {
        from: path.resolve(__dirname, 'src/assets/sounds'),
        to: path.resolve(__dirname, 'dist/assets/sounds'),
    },
];
// don't copy PHP files in webpack server mode, the requests to PHP files will be proxied to Apache
if (process.argv[2] !== 'serve') {
    copyWebpackPluginPatterns.push(
        {
            from: path.resolve(__dirname, 'src/php'),
            to: path.resolve(__dirname, 'dist/api'),
        },
        {
            from: path.resolve(__dirname, 'src/doc/pr'), // html page with stuff for Cross-Check Submit in https://app.rs.school/course/student/cross-check-submit?course=js-2020-q3
            to: path.resolve(__dirname, 'dist/pr'),
        },
    );
}

module.exports = {
    context: path.resolve(__dirname, 'src'),
    mode: 'development',
    devtool: isDevelopmentEnvironment ? 'eval-source-map' : false,
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'templates/index.html',
            filename: 'index.html',
            inject: 'head',
            minify: { collapseWhitespace: isProductionEnvironment },
        }),
        new CopyWebpackPlugin({
            patterns: copyWebpackPluginPatterns,
        }),
        new MiniCssExtractPlugin({ filename: fileName('css') }),
        new ESLintPlugin(),
    ],
    entry: './js/app.js',
    output: {
        filename: fileName('js'),
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/i,
                use: [
                    { loader: MiniCssExtractPlugin.loader, options: { publicPath: '' } },
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(ttf|woff|woff2|eot|svg)$/i,
                use: ['file-loader'],
            },
            {
                test: /\.(jpg|png|ico)$/i,
                use: ['file-loader'],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-proposal-class-properties'],
                    },
                },
            },
        ],
    },
    optimization: {
        moduleIds: 'deterministic',
        splitChunks: {
            chunks: 'all',
        },
    },
    devServer: {
        // https: true,
        port: 12345,
        hot: true,
        proxy: {
            '/api': {
                target: 'http://localhost:12346',
                pathRewrite: { '^/api': '' },
            },
            secure: false,
        },
    },
};
